﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace LiberoRentACarClasses
{
    public class Car
    {
        public Brand Brand { get; private set; }
        public string Model { get; private set; }
        public int Year { get; private set; }
        public int Mileage { get; private set; }
        public double Engine { get; private set; }
        public Color Color { get; set; }
        public string LicensePlate { get; private set; }
        public bool IsAutomatic { get; private set; }
        public int Rank { get; private set; }

        public Car(int brand, string model, int year, int mileage, double engine, int color, string licensePlate, bool isAutomatic, int rank)
        {
            this.Brand = (Brand)brand;

            if (model != "")
                this.Model = model;
            else
                throw new ArgumentOutOfRangeException();

            this.Year = year;

            this.Mileage = mileage; //TODO
            this.Engine = engine; //TODO
            
            this.Color = (Color)color;
            
            if(UtilsValidation.ValidadeLicensePlate(licensePlate))
                this.LicensePlate = licensePlate;
            else
                throw new ArgumentOutOfRangeException();

            this.IsAutomatic = isAutomatic;
            this.Rank = rank;
        }

        public bool IsRent(RentSystem rentSys)
        {
            //Select the return date and the license plate of the 
            //rent list to check if the car`s instance is rent
            var c = rentSys.Rents.Select(x => new { x.ReturnDate, x.Car.LicensePlate }).FirstOrDefault(x => x.ReturnDate > DateTime.Now && x.LicensePlate == this.LicensePlate);

            if (c != null)
                return true;
            else
                return false;
        }

        public override string ToString()
        {
            return string.Format("{0,-6} {1,-10} {2,-5} {3,-3} {4,-7} {5,-5}", this.LicensePlate, this.Brand, this.Model, this.Engine, this.IsAutomatic ? "Automatic" : "Manual",this.Year);
        }
    }
}

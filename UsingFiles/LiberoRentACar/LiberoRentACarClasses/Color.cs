﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses
{
    public enum Color
    {
        Gray,
        Black,
        White,
        Red,
        Blue,
        Green,
        Brown
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses
{
    public abstract class User
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        
        public User(string name, string address, string phone, string email)
        {
            if (name != "")
                this.Name = name;
            else
                throw new ArgumentOutOfRangeException("Your name is required.");

            if(address != "")
                this.Address = address;
            else
                throw new ArgumentOutOfRangeException("Your address is required.");

            if (UtilsValidation.ValidatePhone(phone) && phone!="")
                this.Phone = phone;
            else
                throw new ArgumentOutOfRangeException("Invalid phone number.");

            if (UtilsValidation.ValidateEmail(email) && email != "")
                this.Email = email;
            else
                throw new ArgumentOutOfRangeException("Invalid Email.");
            
        }

    }
}

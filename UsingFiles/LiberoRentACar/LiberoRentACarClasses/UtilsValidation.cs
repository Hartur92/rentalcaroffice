﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace LiberoRentACarClasses
{
    public static class UtilsValidation
    {

        public static string OnlyCharAndNumber(string str)
        {
            StringBuilder newStr = new StringBuilder();
            str = str.ToLower();
            foreach (char ch in str)
            {
                if ((ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'z'))
                    newStr.Append(ch);
                    
            }
            return newStr.ToString();
        }

        public static string OnlyNumber(string str)
        {
            StringBuilder newStr = new StringBuilder();
            str = str.ToLower();
            foreach (char ch in str)
            {
                if (ch >= '0' && ch <= '9')
                    newStr.Append(ch);

            }
            return newStr.ToString();
        }

        public static bool ValidateCpf(string cpf)
        {
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digit;
            int sum;
            int rest;

            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");

            if (cpf.Length != 11)
                return false;

            tempCpf = cpf.Substring(0, 9);
            sum = 0;

            for (int i = 0; i < 9; i++)
                sum += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];

            rest = sum % 11;

            if (rest < 2)
                rest = 0;
            else
                rest = 11 - rest;

            digit = rest.ToString();
            tempCpf = tempCpf + digit;
            sum = 0;

            for (int i = 0; i < 10; i++)
                sum += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];

            rest = sum % 11;

            if (rest < 2)
                rest = 0;
            else
                rest = 11 - rest;

            digit = digit + rest.ToString();

            return cpf.EndsWith(digit);
        }

        public static bool ValidateCnpj(string cnpj)
        {
            int[] multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int soma;
            int resto;
            string digito;
            string tempCnpj;

            cnpj = cnpj.Trim();
            cnpj = cnpj.Replace(".", "").Replace("-", "").Replace("/", "");

            if (cnpj.Length != 14)
                return false;

            tempCnpj = cnpj.Substring(0, 12);
            soma = 0;

            for (int i = 0; i < 12; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];

            resto = (soma % 11);

            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = resto.ToString();
            tempCnpj = tempCnpj + digito;
            soma = 0;

            for (int i = 0; i < 13; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];

            resto = (soma % 11);

            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = digito + resto.ToString();
            return cnpj.EndsWith(digito);
        }

        public static bool ValidateCard(string creditCardNumber)
        {
            if (creditCardNumber != "")
            {
                foreach (char ch in creditCardNumber)
                {
                    if (ch < '0' || ch > '9')
                        return false;
                }
            }
            return true;
        }

        public static bool ValidatePhone(string phone)
        {
            phone = phone.Replace("(", "");
            phone = phone.Replace(")", "");
            phone = phone.Replace(" ", "");
            phone = phone.Replace("-", "");
            phone = phone.Replace("+", "");

            if (phone.Length >= 8 && phone.Length <= 12)
            {
                foreach (char ch in phone)
                {
                    if (ch < '0' || ch > '9')
                        return false;
                }
            }
            else
            {
                return false;
            }

            return true;
        }

        public static bool ValidateEmail(string email)
        {
            if (email != "" && email.Contains("@") && email.Contains(".") && !email.Contains(" "))
            {
                email.Replace("@", "");
                email.Replace(".", "");
                if (email != "")
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public static bool ValidadeLicensePlate(string plate)
        {
            int charCont = 0, numCont = 0;

            foreach (char ch in plate)
            {
                if (charCont > 3 || numCont > 3)
                    return false;
                else if (ch >= '0' && ch <= '9')
                    numCont++;
                else if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'))
                    charCont++;
                else
                    return false;
            }
            return true;
        }
    }
}

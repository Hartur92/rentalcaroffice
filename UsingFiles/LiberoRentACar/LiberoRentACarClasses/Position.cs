﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses
{
    public struct Position
    {
        int latitude;
        int longitude;
        int altitude;

        public Position(int latitude, int longitude, int altitude) : this()
        {
            this.latitude = latitude;
            this.longitude = longitude;
            this.altitude = altitude;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses
{
    public class Client : User
    {
        public string Card { get; set; }
        public bool IsIndividual { get; set; }
        public string IdNum { get; set; }

        public Client(bool isIndividual, string name, string idNum, string address, string phone, string email, string card) : base(name, address, phone, email)
        {
            
            this.IsIndividual = isIndividual;

            if(UtilsValidation.ValidateCard(card))
                this.Card = card;
            else
                throw new ArgumentOutOfRangeException("Invalid Credit Card.");

            if (isIndividual)
            {
                if (UtilsValidation.ValidateCpf(idNum))
                    this.IdNum = idNum;
                else
                    throw new ArgumentOutOfRangeException("Invalid CPF.");
            }
            else
            {
                if (UtilsValidation.ValidateCnpj(idNum))
                    this.IdNum = idNum;
                else
                    throw new ArgumentOutOfRangeException("Invalid CNPJ.");
            }
        }

        public override string ToString()
        {
            return string.Format("{0,-6} {1}", this.IdNum, this.Name);
        }

    }
}

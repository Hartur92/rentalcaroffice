﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses
{
    public class Rent
    {
        public Car Car { get; private set; }
        public Client Client { get; private set; }
        public DateTime PickupDate { get; private set; }
        public DateTime ReturnDate { get; set; }
        public int MileageInit { get; private set; }
        public int MileageFinal { private get; set; }

        public Rent(Car car, Client client, DateTime pickupDate, int days)
        {
            this.Car = car;
            this.Client = client;
            this.PickupDate = pickupDate;
            this.ReturnDate = pickupDate.AddDays(days);
            this.MileageInit = car.Mileage;
        }

        public void returnCar(int mileageFinal)
        {
            if (mileageFinal > this.MileageInit)
                this.MileageFinal = mileageFinal;
            else
                throw new ArgumentOutOfRangeException("Inform a valid final mileage.");
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}",Client.IdNum, Client.Name, Car.LicensePlate, Car.Model);
        }

    }
}

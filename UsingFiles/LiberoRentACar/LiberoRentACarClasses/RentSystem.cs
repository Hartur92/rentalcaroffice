﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace LiberoRentACarClasses
{
    public class RentSystem
    {
        public List<Car> Cars { get; private set; }
        public List<Client> Clients { get; private set; }
        public List<Rent> Rents { get; set; }
        private string carPath = "./CarPersistence.txt";
        private string clientPath = "./ClientPersistence.txt";
        private string rentPath = "./RentPersistence.txt";

        public RentSystem()
        {
            if (File.Exists(carPath) && File.ReadAllText(carPath) != "")
            {
                try
                {
                    this.Cars = JsonConvert.DeserializeObject<List<Car>>(File.ReadAllText(carPath));
                }
                catch (FileNotFoundException)
                {

                    throw;
                }
            }
            else
            {
                this.Cars = new List<Car>();
            }

            if (File.Exists(clientPath) && File.ReadAllText(clientPath) != "")
            {
                try
                {
                    this.Clients = JsonConvert.DeserializeObject<List<Client>>(File.ReadAllText(clientPath));
                }
                catch (FileNotFoundException)
                {
                    
                    throw;
                }
            }
            else
            {
                this.Clients = new List<Client>();
            }

            if (File.Exists(rentPath) && File.ReadAllText(rentPath) != "")
            {
                try
                {
                    this.Rents = JsonConvert.DeserializeObject<List<Rent>>(File.ReadAllText(rentPath));
                }
                catch (FileNotFoundException)
                {

                    throw;
                }
            }
            else
            {
                this.Rents = new List<Rent>();
            }
        }

        public void SaveCar(Car car)
        {
            this.Cars.Add(car);
            string jsonStr = JsonConvert.SerializeObject(this.Cars, Formatting.Indented);
            File.WriteAllText(this.carPath, jsonStr);
        }

        public void SaveClient(Client client)
        {
            this.Clients.Add(client);
            string jsonStr = JsonConvert.SerializeObject(this.Clients, Formatting.Indented);
            File.WriteAllText(this.clientPath, jsonStr);
        }

        public void SaveRent(Rent rent)
        {
            this.Rents.Add(rent);
            string jsonStr = JsonConvert.SerializeObject(this.Rents);
            File.WriteAllText(this.rentPath, jsonStr);
        }

        public void Return(Rent rent, int mileage)
        {
            if (rent.MileageInit >= mileage)
                throw new ArgumentOutOfRangeException("Invalid final mileage.");
            else
            {
                this.Rents.Remove(rent);
                rent.MileageFinal = mileage;
                rent.ReturnDate = DateTime.Now;
                this.Rents.Add(rent);
                string jsonStr = JsonConvert.SerializeObject(this.Rents);
                File.WriteAllText(this.rentPath, jsonStr);
            }
        }


    }
}

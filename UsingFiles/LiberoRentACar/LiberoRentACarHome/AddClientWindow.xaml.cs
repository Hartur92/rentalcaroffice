﻿using LiberoRentACarClasses;
using System;
using System.IO;
using System.Windows;

namespace LiberoRentACarHome
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class AddClientWindow : Window
    {
        private RentSystem rentSys;

        public AddClientWindow(ref RentSystem rentSys)
        {
            InitializeComponent();
            this.rentSys = rentSys;
        }
        //When the radio button "individual" is selected, the labels are set to Name and CPF
        private void RadioButton_Checked_Individual(object sender, RoutedEventArgs e)
        {
            Label_Name.Content = "Name:";
            Label_IdNum.Content = "CPF:";
        }
        //When the radio button "legal" is selected, the labels are set to Enterprise and CNPJ
        private void RadioButton_Checked_Legal(object sender, RoutedEventArgs e)
        {
            Label_Name.Content = "Enterprise:";
            Label_IdNum.Content = "CNPJ:";
        }
        //Button used to get the informations and add to the persistence file
        private void Button_Click_Save(object sender, RoutedEventArgs e)
        {
            //try catch block to treat all the possible exceptions and try to not pass the error to the user
            try
            {
                //Is created a new client's instance and is used the SaveClient method to add the information to the persistence file
                Client client = new Client(RadioButton_Individual.IsChecked.Value, Txt_Name.Text, UtilsValidation.OnlyCharAndNumber(Txt_IdNum.Text), Txt_Address.Text, UtilsValidation.OnlyCharAndNumber(Txt_Phone.Text), Txt_Email.Text, UtilsValidation.OnlyCharAndNumber(Txt_Card.Text));
                this.rentSys.SaveClient(client);
                //Shows a successfully saved message
                MessageBox.Show("Success saved.");
                //After finish the save, the app return to the main menu
                MainWindow mainWindow = new MainWindow();
                mainWindow.Show();
                this.Close();
            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show(ex.ToString());
            }
            //This exception is launched when there's some problem during the validation of the informations into the fields
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show(ex.ParamName.ToString());
            }
        }
        //Button to return to the main menu
        private void Button_Click_MainMenu(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiberoRentACarClasses;

namespace LiberoRentACarHome
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class RentWindow : Window
    {
        private RentSystem rentSys;
        public RentWindow(RentSystem rentSys)
        {
            this.rentSys = rentSys;
            InitializeComponent();
        }

        private void Button_Click_Search(object sender, RoutedEventArgs e)
        {
            //Everytime when the search button is clicked, the list box is cleared
            ListBox_Cars.Items.Clear();
            //Must be implemented the function
            //var cars = rentSys.Cars.SingleOrDefault(x => x.LicensePlate == UtilsValidation.OnlyCharAndNumber(TxtBox_Plate.Text));
            //ListBox_Plate.Items.Add(car.LicensePlate);
            //Sum the rank to search for the right car
            int rank = 0;
            if ((bool)Checkbox_Economic.IsChecked)
                rank += 2;
            if ((bool)Checkbox_Intermediate.IsChecked)
                rank += 4;
            if ((bool)Checkbox_Minivan.IsChecked)
                rank += 8;
            if ((bool)Checkbox_OffRoad.IsChecked)
                rank += 16;
            if ((bool)Checkbox_Premium.IsChecked)
                rank += 32;
            if ((bool)Checkbox_SUV.IsChecked)
                rank += 64;
            //Search the cars that match the characteristics
            var cars = rentSys.Cars.Where(x => ((x.Rank & rank) != 0 || x.LicensePlate == TxtBox_Plate.Text) && !x.IsRent(this.rentSys));
            //Add all the found car
            foreach (var car in cars)
            {
                ListBox_Cars.Items.Add(car);
            }
        }

        private void RadioButton_Cat_Checked(object sender, RoutedEventArgs e)
        {
            TxtBox_Plate.Visibility = System.Windows.Visibility.Hidden;
            Checkbox_Economic.Visibility = System.Windows.Visibility.Visible;
            Checkbox_Intermediate.Visibility = System.Windows.Visibility.Visible;
            Checkbox_Minivan.Visibility = System.Windows.Visibility.Visible;
            Checkbox_SUV.Visibility = System.Windows.Visibility.Visible;
            Checkbox_OffRoad.Visibility = System.Windows.Visibility.Visible;
            Checkbox_Premium.Visibility = System.Windows.Visibility.Visible;
            Label_Search.Content = "Categories:";
            ////<CheckBox x:Name="Checkbox_Economic" Content="Economic" HorizontalAlignment="Left" Margin="112,98,0,0" VerticalAlignment="Top"/>
            //CheckBox Checkbox_Economic = new CheckBox();
            //Checkbox_Economic.Content = "Economic";
            //Checkbox_Economic.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            //Checkbox_Economic.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            //Checkbox_Economic.Margin = new Thickness(112, 98, 0, 0);
            ////<CheckBox x:Name="Checkbox_Intermediate" Content="Intermediate" HorizontalAlignment="Left" Margin="112,123,0,0" VerticalAlignment="Top"/>
            //CheckBox Checkbox_Intermediate = new CheckBox();
            //Checkbox_Intermediate.Content = "Intermediate";
            //Checkbox_Intermediate.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            //Checkbox_Intermediate.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            //Checkbox_Intermediate.Margin = new Thickness(112, 123, 0, 0);
            ////<CheckBox x:Name="Checkbox_Minivan" Content="Minivan" HorizontalAlignment="Left" Margin="220,98,0,0" VerticalAlignment="Top"/>
            //CheckBox Checkbox_Minivan = new CheckBox();
            //Checkbox_Minivan.Content = "Minivan";
            //Checkbox_Minivan.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            //Checkbox_Minivan.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            //Checkbox_Minivan.Margin = new Thickness(220, 98, 0, 0);
            ////<CheckBox x:Name="Checkbox_SUV" Content="SUV" HorizontalAlignment="Left" Margin="220,123,0,0" VerticalAlignment="Top"/>
            //CheckBox Checkbox_SUV = new CheckBox();
            //Checkbox_SUV.Content = "SUV";
            //Checkbox_SUV.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            //Checkbox_SUV.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            //Checkbox_SUV.Margin = new Thickness(220, 123, 0, 0);
            ////<CheckBox x:Name="Checkbox_OffRoad" Content="OffRoad" HorizontalAlignment="Left" Margin="328,98,0,0" VerticalAlignment="Top"/>
            //CheckBox Checkbox_OffRoad = new CheckBox();
            //Checkbox_OffRoad.Content = "OffRoad";
            //Checkbox_OffRoad.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            //Checkbox_OffRoad.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            //Checkbox_OffRoad.Margin = new Thickness(328, 98, 0, 0);
            ////<CheckBox x:Name="Checkbox_Premium" Content="Premium" HorizontalAlignment="Left" Margin="328,123,0,0" VerticalAlignment="Top"/>
            //CheckBox Checkbox_Premium = new CheckBox();
            //Checkbox_Premium.Content = "Premium";
            //Checkbox_Premium.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            //Checkbox_Premium.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            //Checkbox_Premium.Margin = new Thickness(328, 123, 0, 0);
        }

        private void RadioButton_Plate_Checked(object sender, RoutedEventArgs e)
        {
            Label_Search.Content = "License Plate:";
            TxtBox_Plate.Visibility = System.Windows.Visibility.Visible;
            Checkbox_Economic.Visibility = System.Windows.Visibility.Hidden;
            Checkbox_Intermediate.Visibility = System.Windows.Visibility.Hidden;
            Checkbox_Minivan.Visibility = System.Windows.Visibility.Hidden;
            Checkbox_SUV.Visibility = System.Windows.Visibility.Hidden;
            Checkbox_OffRoad.Visibility = System.Windows.Visibility.Hidden;
            Checkbox_Premium.Visibility = System.Windows.Visibility.Hidden;
            //<TextBox x:Name="TxtBox_Plate" HorizontalAlignment="Left" Height="26" Margin="123,95,0,0" TextWrapping="Wrap" Text="" VerticalAlignment="Top" Width="120"/>
            //TextBox TxtBox_Plate = new TextBox();
            //TxtBox_Plate.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            //TxtBox_Plate.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            //TxtBox_Plate.Height = 26;
            //TxtBox_Plate.Width = 120;
            //TxtBox_Plate.Margin = new Thickness(123, 95, 0, 0);
            //TxtBox_Plate.TextWrapping = TextWrapping.Wrap;
            //TxtBox_Plate.Visibility = System.Windows.Visibility.Visible;
            //StackPanel.Children.Add(TxtBox_Plate);
        }

        private void Button_Click_MainMenu(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void Button_Click_SelectClient(object sender, RoutedEventArgs e)
        {
            if (ListBox_Cars.SelectedItem == null)
                MessageBox.Show("Select some car.");
            else
            {
                SelectClientWindow selectClientWindow = new SelectClientWindow(this.rentSys, (Car)ListBox_Cars.SelectedItem);
                selectClientWindow.Show();
                this.Close();
            }
        }
    }
}

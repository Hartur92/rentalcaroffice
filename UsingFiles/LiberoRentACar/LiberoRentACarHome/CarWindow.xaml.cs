﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiberoRentACarClasses;

namespace LiberoRentACarHome
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class CarWindow : Window
    {
        RentSystem rentSys;

        public CarWindow(ref RentSystem rentSys)
        {
            InitializeComponent();
            this.rentSys = rentSys;
        }
        //Button to return to the main menu
        private void Button_Click_MainMenu(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }
        //Button to add a new car
        private void Button_Click_AddCar(object sender, RoutedEventArgs e)
        {
            AddCarWindow addWindow = new AddCarWindow(ref rentSys);
            addWindow.Show();
            this.Close();
        }
        //Button to update car informations
        private void Button_Click_UpdateCar(object sender, RoutedEventArgs e)
        {
            //TODO
        }
    }
}

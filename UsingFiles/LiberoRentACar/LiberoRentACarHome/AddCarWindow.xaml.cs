﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiberoRentACarClasses;
using System.IO;

namespace LiberoRentACarHome
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class AddCarWindow : Window
    {
        RentSystem rentSys;

        public AddCarWindow(ref RentSystem rentSys)
        {
            InitializeComponent();
            this.rentSys = rentSys;
            //force the ComboBox to start with a value
            ComboBox_Color.ItemsSource = Enum.GetValues(typeof(LiberoRentACarClasses.Color));
            ComboBox_Color.SelectedIndex = 0;
            ComboBox_Brand.ItemsSource = Enum.GetValues(typeof(Brand));
            ComboBox_Brand.SelectedIndex = 0;
            //allow only add cars with 10 years or less
            for (int i = -10; i <= 0; i++)
            {
                ComboBox_Year.Items.Add(DateTime.Now.Year + i);
            }
            ComboBox_Year.SelectedIndex = 0;
        }
        //Button to return to the main menu
        private void Button_Click_MainMenu(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }
        //Button to save the information
        private void Button_Click_Save(object sender, RoutedEventArgs e)
        {
            int rank = 0;
            //Sum of the rank dependin on the checkbox selected (id used and bitwise to compare the rak after)
            if ((bool)Checkbox_Economic.IsChecked)
                rank += 2;
            if ((bool)Checkbox_Intermidiate.IsChecked)
                rank += 4;
            if ((bool)Checkbox_Minivan.IsChecked)
                rank += 8;
            if ((bool)Checkbox_OffRoad.IsChecked)
                rank += 16;
            if ((bool)Checkbox_Premium.IsChecked)
                rank += 32;
            if ((bool)Checkbox_SUV.IsChecked)
                rank += 64;
            //try catch block used to avoid pass the exception to the user
            try
            {
                Car car = new Car(int.Parse(ComboBox_Brand.SelectedIndex.ToString()), TxtBox_Model.Text, int.Parse(ComboBox_Year.SelectedItem.ToString()), int.Parse(TxtBox_Mileage.Text), double.Parse(TxtBox_Engine.Text), int.Parse(ComboBox_Color.SelectedIndex.ToString()), UtilsValidation.OnlyCharAndNumber(TxtBox_LicensePlate.Text), Checkbox_Automatic.IsChecked.Value, rank);
                rentSys.SaveCar(car);
                //After save the car, is showed a message box and goes to the main menu window
                MessageBox.Show("Success saved.");
                MainWindow mainWindow = new MainWindow();
                mainWindow.Show();
                this.Close();
            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show(ex.ToString());
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show(ex.ParamName.ToString());
            }
        }
    }
}

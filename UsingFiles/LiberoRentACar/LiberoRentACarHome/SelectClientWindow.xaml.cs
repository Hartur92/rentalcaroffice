﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LiberoRentACarClasses;

namespace LiberoRentACarHome
{
    /// <summary>
    /// Interaction logic for SelectClientWindow.xaml
    /// </summary>
    public partial class SelectClientWindow : Window
    {
        private RentSystem rentSys;
        private Car car;

        public SelectClientWindow(RentSystem rentsys, Car car)
        {
            this.rentSys = rentsys;
            this.car = car;
            
            InitializeComponent();

            for (int i = 1; i < 31; i++)
            {
                ComboBox_Time.Items.Add(i);
            }

            ComboBox_Time.SelectedIndex = 0;
        }

        private void Button_Click_MainMenu(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void Button_Click_Search(object sender, RoutedEventArgs e)
        {
            ListBox_Clients.Items.Clear();

            var clients = rentSys.Clients.Where(x => (x.Name == TxtBox_Name.Text || x.IdNum == TxtBox_CPF.Text));
            foreach (var client in clients)
            {
                ListBox_Clients.Items.Add(client);
            }
        }

        private void Button_Click_Rent(object sender, RoutedEventArgs e)
        {
            if (ListBox_Clients.SelectedItem == null)
                MessageBox.Show("Select some client.");
            else
            {
                Rent rent = new Rent(this.car, (Client)ListBox_Clients.SelectedItem, DateTime.Now, int.Parse(ComboBox_Time.SelectedItem.ToString()));
                rentSys.SaveRent(rent);
                MessageBox.Show("The car was successfully rent.");
            }

            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
            
        }
    }
}

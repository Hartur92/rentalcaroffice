﻿using System.Windows;
using LiberoRentACarClasses;

namespace LiberoRentACarHome
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Object used to intermediate the change of information between the UI and the core
        private RentSystem rentSys;

        public MainWindow()
        {
            InitializeComponent();
            //The constructor of RentSystem loads all the information of the cars, clients and rents.
            //This is used to avoid the use of the file
            this.rentSys = new RentSystem();
        }
        //Button to acces the client menu
        private void Button_Click_Client(object sender, RoutedEventArgs e)
        {
            //The object rentSys is passed to the other windows to help the data manipulation and avoid the use of the file
            //The object is sent as a reference to avoid memory consumption
            ClientWindow clientWindow = new ClientWindow(ref this.rentSys);
            clientWindow.Show();
            this.Close();
        }
        //Button to acces the car menu
        private void Button_Click_Car(object sender, RoutedEventArgs e)
        {
            CarWindow carWindow = new CarWindow(ref this.rentSys);
            carWindow.Show();
            this.Close();
        }
        //Button to start a rent process
        private void Button_Click_Rent(object sender, RoutedEventArgs e)
        {
            RentWindow rentWindow = new RentWindow(this.rentSys);
            rentWindow.Show();
            this.Close();
        }
        //Button to start a return process
        private void Button_Click_Return(object sender, RoutedEventArgs e)
        {
            ReturnWindow returnWindow = new ReturnWindow(this.rentSys);
            returnWindow.Show();
            this.Close(); ;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiberoRentACarClasses;

namespace LiberoRentACarHome
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ReturnWindow : Window
    {
        RentSystem rentSys;

        public ReturnWindow(RentSystem rentSys)
        {
            this.rentSys = rentSys;
            InitializeComponent();
        }

        private void Button_Click_Search(object sender, RoutedEventArgs e)
        {
            ListBox_Rents.Items.Clear();

            var rents = rentSys.Rents.Where(x => (x.Client.Name == TxtBox_Name.Text || x.Client.IdNum == TxtBox_CPF.Text) && x.ReturnDate >= DateTime.Today);
            foreach (var rent in rents)
            {
                ListBox_Rents.Items.Add(rent);
            }
        }

        private void Button_Click_Return(object sender, RoutedEventArgs e)
        {
            if (TxtBox_Mileage.Text == "" || ListBox_Rents.SelectedItem == null)
                MessageBox.Show("Fill the fields.");
            else
            {
                try
                {
                    this.rentSys.Return((Rent)ListBox_Rents.SelectedItem, int.Parse(TxtBox_Mileage.Text));
                    MessageBox.Show("Successfully returned.");
                    MainWindow mainWindow = new MainWindow();
                    mainWindow.Show();
                    this.Close();
                }
                catch (ArgumentOutOfRangeException ex) 
                {
                    MessageBox.Show(ex.ParamName.ToString());
                }
            }

        }

        private void Button_Click_MainMenu(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void TxtBox_Mileage_TextChanged_Mileage(object sender, TextChangedEventArgs e)
        {
            TxtBox_Mileage.Text = UtilsValidation.OnlyNumber(TxtBox_Mileage.Text);
        }
    }
}

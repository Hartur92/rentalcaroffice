﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiberoRentACarClasses.Interfaces;

namespace LiberoRentACarClasses.DBAccess
{
    public class ModelRepository : IModelRepository
    {

        public Model GetModel(string name)
        {
            using (RentSysDbContext rentSys = new RentSysDbContext())
            {
                return rentSys.Models.Include("Manufacturer").FirstOrDefault(x => x.Name == name);
            }
        }

        public List<string> GetModelByManufacturer(string manufacturer)
        {
            using (RentSysDbContext rentSys = new RentSysDbContext())
            {
                return rentSys.Models.OrderBy(x => x.Name).Where(x => x.Manufacturer.Name == manufacturer).Select(x => x.Name).ToList();
            }
        }

        public void Add(Model model)
        {
            using (RentSysDbContext rentSys = new RentSysDbContext())
            {
                rentSys.Models.Add(model);
                rentSys.SaveChanges();
            }
        }

        public void Update(string oldName, Model model)
        {
            using (RentSysDbContext rentSys = new RentSysDbContext())
            {
                Model oldModel = rentSys.Models.FirstOrDefault(x => x.Name == oldName);
                oldModel.Engine = model.Engine;
                oldModel.ManufacturerID = model.ManufacturerID;
                oldModel.Name = model.Name;
                rentSys.SaveChanges();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiberoRentACarClasses.Interfaces;

namespace LiberoRentACarClasses.DBAccess
{
    public class ManufacturersRepository : IManufacturersRepository
    {
        public List<string> GetManufacturers()
        {
            using (RentSysDbContext rentSys = new RentSysDbContext())
            {
                return rentSys.Manufacturers.OrderBy(x => x.Name).Select(x => x.Name).ToList();
            }
        }


        public void Add(Manufacturer man)
        {
            using (RentSysDbContext rentSys = new RentSysDbContext())
            {
                rentSys.Manufacturers.Add(man);
                rentSys.SaveChanges();
            }
        }

        public bool contain(string name)
        {
            using (RentSysDbContext rentSys = new RentSysDbContext())
            {
                return rentSys.Manufacturers.Any(x => x.Name == name);
            }
        }


        public int GetManufacturerID(string name)
        {
            using (RentSysDbContext rentSys = new RentSysDbContext())
            {
                return rentSys.Manufacturers.FirstOrDefault(x => x.Name == name).ManufacturerID;
            }
        }

        public void Update(int manufacturerID, string newName)
        {
            using (RentSysDbContext rentSys = new RentSysDbContext())
            {
                Manufacturer man = rentSys.Manufacturers.FirstOrDefault(x => x.ManufacturerID == manufacturerID);
                man.Name = newName;
                rentSys.SaveChanges();
            }
        }
    }
}

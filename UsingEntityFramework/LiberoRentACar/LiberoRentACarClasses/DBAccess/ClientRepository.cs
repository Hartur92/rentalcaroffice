﻿using LiberoRentACarClasses.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses.DBAccess
{
    public class ClientRepository : IClientRepository
    {

        public void Add(Client client)
        {
            using (var rentSys = new RentSysDbContext())
            {
                rentSys.Users.Add(client);
                rentSys.SaveChanges();
            }
        }

        public bool Contains(string idNum)
        {
            using (var rentSys = new RentSysDbContext())
            {
                return rentSys.Users.OfType<Client>().Any(x => x.IdNum == idNum);
            }
        }

        public List<Client> GetClients(string name, string idNum)
        {
            using (var rentSys = new RentSysDbContext())
            {
                return rentSys.Users.OfType<Client>().Where(x => (x.Name == name || x.IdNum == idNum)).ToList();
            }
        }

        public Client GetClient(int id)
        {
            using (RentSysDbContext rentSys = new RentSysDbContext())
            {
                return rentSys.Users.OfType<Client>().FirstOrDefault(x => x.UserID == id);
            }
        }

        public void Update(Client client)
        {
            using (RentSysDbContext rentSys = new RentSysDbContext())
            {
                Client old = rentSys.Users.OfType<Client>().FirstOrDefault(x => x.IdNum == client.IdNum);
                old.Address = client.Address;
                old.Card = client.Card;
                old.Email = client.Email;
                old.Name = client.Name;
                old.Phone = client.Phone;
                rentSys.SaveChanges();
            }
        }
    }
}

﻿using LiberoRentACarClasses;
using LiberoRentACarClasses.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace LiberoRentACarClasses.DBAccess
{
    public class CarRepository : ICarRepository
    {
        public CarRepository()
        {
        }

        public void Add(Car car)
        {
            using (RentSysDbContext rentSys = new RentSysDbContext())
            {
                rentSys.Cars.Add(car);
                rentSys.SaveChanges();
            }
        }

        public Car Get(int id)
        {
            using (RentSysDbContext rentSys = new RentSysDbContext())
            {
                return rentSys.Cars.Include("Model").Include("Model.Manufacturer").FirstOrDefault(x => x.CarID == id);
            }
        }

        public bool Contains(string licensePlate)
        {
            using (RentSysDbContext rentSys = new RentSysDbContext())
            {
                return rentSys.Cars.FirstOrDefault(x => x.LicensePlate == licensePlate) != null;
            }
        }

        public bool Contains(int id)
        {
            using (RentSysDbContext rentSys = new RentSysDbContext())
            {
                return rentSys.Cars.FirstOrDefault(x => x.CarID == id) != null;
            }
        }

        public void Update(Car car)
        {
            using (RentSysDbContext rentSys = new RentSysDbContext())
            {
                Car old = rentSys.Cars.FirstOrDefault(x => x.LicensePlate == car.LicensePlate);
                
                old.IsAutomatic = car.IsAutomatic;
                old.Mileage = car.Mileage;
                old.ModelID = car.ModelID;
                old.Rank = car.Rank;
                old.Year = car.Year;
                old.Price = car.Price;

                rentSys.SaveChanges();
            }
        }

        public List<Car> Get(int rank, string licensePlate)
        {
            using (RentSysDbContext rentSys = new RentSysDbContext())
            {
                var teste = rentSys.Cars.Include("Model").Where(x => (((x.Rank & rank) != 0) || x.LicensePlate == licensePlate)).ToList();
                return rentSys.Cars.Include("Model").Where(x => (((x.Rank & rank) != 0) || x.LicensePlate == licensePlate)).ToList();
            }
        }
    }
}

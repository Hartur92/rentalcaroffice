﻿using LiberoRentACarClasses;
using LiberoRentACarClasses.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses.DBAccess
{
    class RentRepository : IRentRepository
    {
        public List<string> GetRentsInGoing()
        {
            using (RentSysDbContext rentSys = new RentSysDbContext())
            {
                return rentSys.Rents.Where(x => x.ReturnDate > DateTime.Now).Select(x => x.Car.LicensePlate).ToList();
            }
        }

        public List<Car> GetCars(int rank, string licensePlate)
        {
            using (RentSysDbContext rentSys = new RentSysDbContext())
            {
                return rentSys.Cars.Include("Model").Where(x => (((x.Rank & rank) != 0 || x.LicensePlate == licensePlate))).ToList();
            }
        }

        public void Add(Rent rent)
        {
            using (RentSysDbContext rentSys = new RentSysDbContext())
            {
                rentSys.Rents.Add(rent);
                rentSys.SaveChanges();
            }
        }

        public List<Rent> SearchRent(string clientName, string idNum)
        {
            using (RentSysDbContext rentSys = new RentSysDbContext())
            {
                return rentSys.Rents.Include("Client").Include("Car").Include("Car.Model").Where(x => (x.Client.Name == clientName || x.Client.IdNum == idNum) && x.ReturnDate >= DateTime.Now).ToList();
            }
        }

        public void Return(Rent rent, int mileage)
        {
            using (RentSysDbContext rentSys = new RentSysDbContext())
            {
                Rent rentDb = rentSys.Rents.FirstOrDefault(x => x.RentID == rent.RentID);
                rentDb.ReturnDate = DateTime.Now;
                rentDb.Price = rentDb.Car.Price * ((DateTime.Now - rentDb.PickupDate).Days + 1);
                rentSys.Cars.FirstOrDefault(x => x.CarID == rent.CarID).Mileage = mileage;
                rentSys.SaveChanges();
            }
        }
    }
}

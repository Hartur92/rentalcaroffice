﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using LiberoRentACarClasses;

namespace LiberoRentACarClasses.Interfaces
{
    public interface IRentSysDbContext
    {
        DbSet<Car> Cars { get; set; }
        DbSet<User> Users { get; set; }
        DbSet<Rent> Rents { get; set; }
        DbSet<Manufacturer> Manufacturers { get; set; }
        DbSet<Model> Models { get; set; }

        int SaveChanges();

    }
}

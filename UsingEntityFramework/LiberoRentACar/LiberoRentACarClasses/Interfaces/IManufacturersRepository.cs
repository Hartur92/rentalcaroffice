﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses.Interfaces
{
    public interface IManufacturersRepository
    {
        List<string> GetManufacturers();
        void Add(Manufacturer man);
        bool contain(string name);

        int GetManufacturerID(string name);

        void Update(int manufacturerID, string newName);
    }
}

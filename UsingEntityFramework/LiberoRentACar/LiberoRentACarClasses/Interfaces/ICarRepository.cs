﻿using LiberoRentACarClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses.Interfaces
{
    public interface ICarRepository
    {
        void Add(Car car);

        Car Get(int id);

        bool Contains(string licensePlate);

        bool Contains(int id);

        void Update(Car car);

        List<Car> Get(int rank, string licensePlate);
    }
}

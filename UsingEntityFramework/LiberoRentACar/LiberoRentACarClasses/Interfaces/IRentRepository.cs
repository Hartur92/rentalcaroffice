﻿using LiberoRentACarClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses.Interfaces
{
    public interface IRentRepository
    {
        List<string> GetRentsInGoing();
        List<Car> GetCars(int rank, string licensePlate);
        void Add(Rent rent);
        List<Rent> SearchRent(string clientName, string idNum);
        void Return(Rent rent, int mileage);
    }
}

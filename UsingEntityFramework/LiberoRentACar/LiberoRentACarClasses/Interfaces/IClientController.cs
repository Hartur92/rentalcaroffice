﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses.Interfaces
{
    public interface IClientController
    {
        bool SaveClient(bool isIndividual, string name, string idNum, string address, string phone, string email, string card);
        List<Client> SearchClient(string name, string idNum);
        Client GetClient(int id);
        void UpdateClient(bool isIndividual, string name, string idNum, string address, string phone, string email, string card);
        
    }
}

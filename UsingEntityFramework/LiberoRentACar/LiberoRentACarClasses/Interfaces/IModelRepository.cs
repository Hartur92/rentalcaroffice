﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses.Interfaces
{
    public interface IModelRepository
    {
        Model GetModel(string name);
        List<string> GetModelByManufacturer(string manufacturer);
        void Add(Model model);

        void Update(string oldName, Model model);
    }
}

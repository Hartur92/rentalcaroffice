﻿using LiberoRentACarClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses.Interfaces
{
    public interface ICarController
    {
        Array GetColors();
        bool SaveCar(string model, string year, string mileage, string color, string licensePlate, bool isAutomatic, int rank, string price);
        void UpdateCar(string model, string year, string mileage, string color, string licensePlate, bool isAutomatic, int rank, string price);
        Car GetCar(int id);
        List<Car> GetCars(int rank, string licensePlate);
    }
}

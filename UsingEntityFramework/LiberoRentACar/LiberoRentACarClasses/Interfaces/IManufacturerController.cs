﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses.Interfaces
{
    public interface IManufacturerController
    {
        void Add(string name);
        List<string> GetManufacturers();

        void Update(string oldName, string newName);
    }
}

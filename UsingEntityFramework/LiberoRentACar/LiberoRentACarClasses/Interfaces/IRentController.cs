﻿using LiberoRentACarClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses.Interfaces
{
    public interface IRentController
    {
        List<Car> SearchNotRentedCars(int rank, string licensePlate);
        void SaveRent(int userID, int carID, int days);
        List<Rent> SearchRents(string name, string idNum);
        void Return(Rent rent, string mileage);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses.Interfaces
{
    public interface IModelController
    {
        List<string> GetManufacturers();
        List<string> GetModels(string manufacturer);
        void Add(int manufacturerID, string name, string engine);

        Model GetModel(string name);

        void Update(string oldName, string name, string engine, string manufacturer);
    }
}

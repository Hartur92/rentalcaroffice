﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses.Interfaces
{
    public interface IClientRepository
    {

        bool Contains(string idNum);
        void Add(Client client);
        List<Client> GetClients(string name, string idNum);
        Client GetClient(int id);
        void Update(Client client);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses
{
    public class Model
    {
        public virtual Manufacturer Manufacturer { get; set; }
        public int ManufacturerID { get; set; }

        public int ModelID { get; set; }
        public string Name { get; set; }
        public double Engine { get; set; }

        public Model(){}

        public Model(int manufacturerID, string name, double engine)
        {
            this.ManufacturerID = manufacturerID;
            this.Name = name;
            this.Engine = engine;
        }
    }
}

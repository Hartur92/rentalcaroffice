﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses
{
    public class Client : User
    {
        //Client extends User so, it doesn't need an ID property
        public string Card { get; set; }
        public bool IsIndividual { get; set; }
        public string IdNum { get; set; }
        //empty constructor to be used by the RentSysDbContext
        public Client() : base() { }

        public Client(bool isIndividual, string name, string idNum, string address, string phone, string email, string card) : base(name, address, phone, email)
        {
            this.IsIndividual = isIndividual;
            this.Card = card;
            this.IdNum = idNum;
        }

        public override string ToString()
        {
            return string.Format("{0,-6} {1}", this.IdNum, this.Name);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses
{
    public abstract class User
    {
        public int UserID { get; set; }

        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        //empty constructor to be used by the RentSysDbContext
        public User() { }

        public User(string name, string address, string phone, string email)
        {
            this.Name = name;
            this.Address = address;
            this.Phone = phone;
            this.Email = email;
            
        }

    }
}

﻿using LiberoRentACarClasses.Controllers;
using LiberoRentACarClasses.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses
{
    public class Rent
    {

        //RentSysDbContext ID
        public int RentID { get; set; }
        //FK to Car
        public virtual Car Car { get; set; }
        public int CarID { get; set; }
        //FK to Client
        public virtual Client Client { get; set; }
        public int UserID { get; set; }

        public DateTime PickupDate { get; set; }
        public DateTime ReturnDate { get; set; }
        public int MileageInit { get; private set; }
        public int MileageFinal { private get; set; }

        public decimal Price { get; set; }
        //empty constructor to be used by the RentSysDbContext
        public Rent() { }

        public Rent(int carID, int userID, DateTime pickupDate, DateTime returnDate)
        {
            ICarController carController = new CarController();
            this.CarID = carID;
            this.UserID = userID;
            this.PickupDate = pickupDate;
            this.ReturnDate = returnDate;
            this.Price = (returnDate - pickupDate).Days * carController.GetCar(carID).Price;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", this.Client.IdNum, this.Client.Name, this.Car.LicensePlate, this.Car.Model.Name);
        }

    }
}

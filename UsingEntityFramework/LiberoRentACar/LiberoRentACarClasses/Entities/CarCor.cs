﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses
{
    public enum CarCor
    {
        Gray,
        Black,
        White,
        Red,
        Blue,
        Green,
        Brown
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Data.Entity;
using LiberoRentACarClasses.Interfaces;
using LiberoRentACarClasses.Controllers;
using LiberoRentACarClasses.DBAccess;

namespace LiberoRentACarClasses
{
    public class RentSysInitializer : DropCreateDatabaseIfModelChanges<RentSysDbContext>
    {
        protected override void Seed(RentSysDbContext rentSys)
        {
            base.Seed(rentSys);

            IClientController clientController = new ClientController();
            clientController.SaveClient(true, "Hartur", "96254839857", "Address1", "92471119", "hartur@ecomp.poli.br", "456456456");
            clientController.SaveClient(true, "Henrique", "28811516471", "Address2", "92472119", "herique@ecomp.poli.br", "456456456");
            clientController.SaveClient(true, "Felipe", "86731826545", "Address3", "92473119", "felipe@ecomp.poli.br", "456456456");
            clientController.SaveClient(true, "Thiago", "77711677642", "Address4", "92474119", "thiago@ecomp.poli.br", "456456456");
            clientController.SaveClient(true, "Eduardo", "38304886103", "Address5", "92475119", "eduardo@cin.ufpe.br", "456456456");

            IManufacturerController manufacturerController = new ManufacturerController();
            manufacturerController.Add("Chevrolet");
            manufacturerController.Add("Volkswagen");
            manufacturerController.Add("Fiat");
            manufacturerController.Add("Jeep");
            manufacturerController.Add("Ford");
            manufacturerController.Add("Honda");
            manufacturerController.Add("Toyota");

            rentSys.SaveChanges();

            IModelController modelController = new ModelController();
            modelController.Add(1, "Onix 1.0", "1");
            modelController.Add(1, "Onix 1.4", "1.4");
            modelController.Add(1, "S10", "2");

            modelController.Add(2, "Jetta", "2");
            modelController.Add(2, "Gol 1.0", "1");
            modelController.Add(2, "Gol 1.6", "1");

            modelController.Add(3, "Bravo T-Jet", "1.4");
            modelController.Add(3, "Punto", "1.4");
            modelController.Add(3, "Freemont", "2");
            modelController.Add(3, "Bravo", "2");
            modelController.Add(3, "Uno 1.0", "1");
            modelController.Add(3, "Uno 1.4", "1.4");

            modelController.Add(4, "Renegade", "2");
            modelController.Add(4, "Renegade Sport", "2");
            modelController.Add(4, "Cherokee", "2.5");

            modelController.Add(5, "Ka", "1.5");
            modelController.Add(5, "New Fiesta", "1.5");
            modelController.Add(5, "Fusion", "2");
            modelController.Add(5, "Ranger", "2");

            modelController.Add(6, "Fit", "1.4");
            modelController.Add(6, "Civic", "2");
            modelController.Add(6, "HRV", "2");

            modelController.Add(7, "Hilux", "2");
            modelController.Add(7, "Hilux SW4", "2");
            modelController.Add(7, "Corola", "2");

            ICarController carController = new CarController();
            carController.SaveCar(
                    "Fit",
                    "2015",
                    "5000",
                    ((int)CarCor.Gray).ToString(),
                    "qwe1234",
                    true,
                    ((int)Category.Intermidiate + (int)Category.Minivan),
                    "80"
            );

            carController.SaveCar(
                    "Civic",
                    "2015",
                    "100",
                    ((int)CarCor.Gray).ToString(),
                    "qwe1235",
                    true,
                    ((int)Category.Premium),
                    "120"
            );

            carController.SaveCar(
                    "HRV",
                    "2015",
                    "5000",
                    ((int)CarCor.Gray).ToString(),
                    "qwe1236",
                    true,
                    ((int)Category.Premium + (int)Category.SUV),
                    "180"
            );

            rentSys.SaveChanges();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses
{

    public class Manufacturer{
        public string Name {get; set;}
        public int ManufacturerID { get; set; }

        public virtual List<Model> Models { get; set; }

        public Manufacturer() { }

        public Manufacturer(string name)
        {
            this.Name = name;
        }
    }
}

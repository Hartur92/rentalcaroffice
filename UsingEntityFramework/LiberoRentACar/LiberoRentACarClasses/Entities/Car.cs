﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.ComponentModel.DataAnnotations.Schema;

namespace LiberoRentACarClasses
{
    public class Car
    {

        public int CarID { get; set; }

        //public virtual Manufacturer Manufacturer { get; set; }
        //public int ManufacturerID { get; set; }

        public virtual Model Model { get; set; }
        public int ModelID { get; set; }

        public int Year { get; set; }
        public int Mileage { get; set; }
        
        public CarCor CarCor { get; set; }
        public string LicensePlate { get; set; }
        public bool IsAutomatic { get; set; }
        public int Rank { get; set; }

        public decimal Price { get; set; }
        
        //empty constructor to be used by the RentSysDbContext
        public Car() { }

        public Car(int modelID, int year, int mileage, CarCor color, string licensePlate, bool isAutomatic, int rank, decimal price)
        {
            this.ModelID = modelID;
            this.Year = year;
            this.Mileage = mileage;
            this.CarCor = color;
            this.LicensePlate = licensePlate;
            this.IsAutomatic = isAutomatic;
            this.Rank = rank;
            this.Price = price;
        }
        

        public override string ToString()
        {
            return string.Format("{0,-8} {1,-10} {2,-5} {3,-5} R${4}", this.LicensePlate, this.Model.Name, this.IsAutomatic ? "Automatic" : "Manual", this.Year, this.Price.ToString());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses
{

    [Flags]
    public enum Category
    {
        Economic = 2,
        Intermidiate = 4,
        Minivan = 8,
        OffRoad = 16,
        Premium = 32,
        SUV = 64
    }

    public static class EnumCarRank
    {
        public static string GetCategories(this Car car)
        {
            int rank = (int)car.Rank;
            return ((Category)rank).ToString();
        }
    }



}

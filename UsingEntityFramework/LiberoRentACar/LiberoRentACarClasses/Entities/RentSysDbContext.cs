﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Data.Entity;
using LiberoRentACarClasses.Interfaces;
using LiberoRentACarClasses;


namespace LiberoRentACarClasses
{

    public class RentSysDbContext : DbContext, IRentSysDbContext
    {
        public DbSet<Car> Cars { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Rent> Rents { get; set; }
        public DbSet<Manufacturer> Manufacturers { get; set; }
        public DbSet<Model> Models { get; set; }


        public override int SaveChanges()
        {
            return base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            Database.SetInitializer<RentSysDbContext>(new RentSysInitializer());

            modelBuilder.Entity<Manufacturer>().HasMany<Model>(t => t.Models).WithRequired(t => t.Manufacturer).HasForeignKey(t => t.ManufacturerID).WillCascadeOnDelete(false);
            
            modelBuilder.Entity<Client>().ToTable("Clients");

        }

    }
}

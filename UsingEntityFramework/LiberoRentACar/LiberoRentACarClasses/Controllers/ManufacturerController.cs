﻿using LiberoRentACarClasses.DBAccess;
using LiberoRentACarClasses.Interfaces;
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses.Controllers
{
    public class ManufacturerController : IManufacturerController
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        IManufacturersRepository manRepo;

        public ManufacturerController()
        {
            this.manRepo = new ManufacturersRepository();
        }

        public List<string> GetManufacturers()
        {
            return this.manRepo.GetManufacturers();
        }

        public void Add(string name)
        {
            XmlConfigurator.Configure();
            if (name != "")
            {
                if (!manRepo.contain(name))
                {
                    manRepo.Add(new Manufacturer(name));
                    log.Info(string.Format("A new Manufacturer named {0} was added.", name));
                }
                else
                {
                    log.Error(string.Format("An exception was launched because the name choosed by the user to create a new Manufacturer ({0}) already exists", name));
                    throw new ArgumentOutOfRangeException("This manufacturer already exists.");
                }
            }
            else
            {
                log.Error(string.Format("An exception was launched because the user's manufacturer ({0}) name was invalid.", name));
                throw new ArgumentOutOfRangeException("Invalid name.");
            }
        }


        public void Update(string oldName, string newName)
        {
            manRepo.Update(manRepo.GetManufacturerID(oldName), newName);
        }
    }
}

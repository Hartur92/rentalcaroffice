﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiberoRentACarClasses.Interfaces;
using LiberoRentACarClasses.DBAccess;
using System.Text.RegularExpressions;
using System.Reflection;
using log4net;
using log4net.Config;

namespace LiberoRentACarClasses.Controllers
{
    public class ClientController : IClientController
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        IClientRepository clientRepo;

        public ClientController()
        {
            this.clientRepo = new ClientRepository();
        }

        public bool SaveClient(bool isIndividual, string name, string idNum, string address, string phone, string email, string card)
        {
            XmlConfigurator.Configure();
            try
            {
                this.ValidateClient(isIndividual, name, idNum, address, phone, email, card);
                if (!clientRepo.Contains(idNum))
                {
                    clientRepo.Add(new Client(isIndividual, name, idNum, address, phone, email, card));
                    log.Info(string.Format("A client with IdNumber {0} whas added.", idNum));
                    return true;
                }
                else
                {
                    log.Error(string.Format("An exception was launched because the user tryed to add a new client with an existing cpf."));
                    throw new ArgumentOutOfRangeException("This CPF was already registered.");
                }
            }
            catch(ArgumentOutOfRangeException)
            {
                throw;
            }
        }

        public bool ValidateClient(bool isIndividual, string name, string idNum, string address, string phone, string email, string card)
        {
            XmlConfigurator.Configure();
            if (!this.ValidateName(name))
            {
                log.Error(string.Format("An exception was launched because the user's name input ({0}) was invalid.", name));
                throw new ArgumentOutOfRangeException("Put a valid name.");
            }

            if (isIndividual)
            {
                if (!this.ValidateCpf(idNum))
                {
                    log.Error(string.Format("An exception was launched because the user's CPF input ({0}) was invalid.", idNum));
                    throw new ArgumentOutOfRangeException("Put a valid CPF.");
                }
            }
            else
            {
                if (!this.ValidateCnpj(idNum))
                {
                    log.Error(string.Format("An exception was launched because the user's CNPJ input ({0}) was invalid.", idNum));
                    throw new ArgumentOutOfRangeException("Put a valid CNPJ.");
                }
            }

            if (!this.ValidatePhone(phone))
            {
                log.Error(string.Format("An exception was launched because the user's phone number input ({0}) was invalid.", phone));
                throw new ArgumentOutOfRangeException("Put a valid phone number.");
            }

            if (!this.ValidateEmail(email))
            {
                log.Error(string.Format("An exception was launched because the user's email input ({0}) was invalid.", email));
                throw new ArgumentOutOfRangeException("Put a valid e-mail.");
            }

            if (!this.ValidateCard(card))
            {
                log.Error(string.Format("An exception was launched because the user's credit card number input ({0}) was invalid.", card));
                throw new ArgumentOutOfRangeException("Put a valid card.");
            }

            if (name == "" || address == "")
            {
                log.Error("An exception was launched because the user didn't fill all the fields while adding a new Client.");
                throw new ArgumentOutOfRangeException("Fill all the fields.");
            }

            return true;
            
        }

        public bool ValidateName(string name)
        {
            foreach (char ch in name)
            {
                if ((ch < 'a' || ch > 'z') && (ch < 'A' || ch > 'Z') && ch!=' ' && ch!='\0')
                {
                    return false;
                }
            }
            return true;
        }

        private bool ValidateCpf(string cpf)
        {
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digit;
            int sum;
            int rest;

            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");

            foreach (char ch in cpf)
            {
                if (ch < '0' || ch > '9')
                {
                    return false;
                }
            }

            if (cpf.Length != 11)
                return false;

            tempCpf = cpf.Substring(0, 9);
            sum = 0;

            for (int i = 0; i < 9; i++)
                sum += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];

            rest = sum % 11;

            if (rest < 2)
                rest = 0;
            else
                rest = 11 - rest;

            digit = rest.ToString();
            tempCpf = tempCpf + digit;
            sum = 0;

            for (int i = 0; i < 10; i++)
                sum += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];

            rest = sum % 11;

            if (rest < 2)
                rest = 0;
            else
                rest = 11 - rest;

            digit = digit + rest.ToString();

            return cpf.EndsWith(digit);
        }

        private bool ValidateCnpj(string cnpj)
        {
            cnpj = cnpj.Replace(".", "");
            cnpj = cnpj.Replace("-", "");

            foreach (char ch in cnpj)
            {
                if (ch < '0' || ch > '9')
                {
                    return false;
                }
            }

            if (cnpj != "")
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }

        private bool ValidateCard(string card)
        {
            card = card.Replace(".", "");
            card = card.Replace("-", "");

            foreach (char ch in card)
            {
                if (ch < '0' || ch > '9')
                {
                    return false;
                }
            }

            if (card != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool ValidatePhone(string phone)
        {
            phone = phone.Replace(" ", "");
            phone = phone.Replace("+", "");
            phone = phone.Replace("(", "");
            phone = phone.Replace(")", "");
            phone = phone.Replace("-", "");

            foreach (char ch in phone)
            {
                if (ch < '0' || ch > '9')
                {
                    return false;
                }
            }

            if (phone.Length >= 8 && phone.Length <= 12)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool ValidateEmail(string email)
        {
            if (email != "" && email.Contains("@") && email.Contains(".") && !email.Contains(" "))
            {
                //validates the email based on a state machine for:
                //  'ch'+'@''ch'+('.''ch'+)+
                int state = 0;
                int i = 1;

                foreach (char c in email)
                {
                    switch (state)
                    {
                        case 0:
                            if (c < 'a' || c > 'z')
                            {
                                return false;
                            }
                            else
                            {
                                state = 1;
                            }
                            break;
                        case 1:
                            if (c == '@')
                            {
                                state = 2;
                            }
                            break;
                        case 2:
                            if (c < 'a' || c > 'z')
                            {
                                return false;
                            }
                            else
                            {
                                state = 3;
                            }
                            break;
                        case 3:
                            if (c == '.')
                            {
                                state = 4;
                            }
                            break;
                        case 4:
                            if (c < 'a' || c > 'z')
                            {
                                return false;
                            }
                            else
                            {
                                state = 5;
                            }
                            break;
                        case 5:
                            if (c == '.')
                            {
                                state = 6;
                            }
                            else if (i == email.Length)
                            {
                                return true;
                            }
                            break;
                        case 6:
                            if (c < 'a' || c > 'z')
                            {
                                return false;
                            }
                            else if (c == '.')
                            {
                                state = 5;
                            }
                            else if (i == email.Length)
                            {
                                return true;
                            }
                            break;
                        default:
                            return false;
                    }
                    i++;
                }
                return false;
            }
            else
            {
                return false;
            }

        }

        public List<Client> SearchClient(string name, string idNum)
        {
            return clientRepo.GetClients(name, idNum);
        }

        public Client GetClient(int id)
        {
            return clientRepo.GetClient(id);
        }

        public void UpdateClient(bool isIndividual, string name, string idNum, string address, string phone, string email, string card)
        {
            XmlConfigurator.Configure();
            try
            {
                this.ValidateClient(isIndividual, name, idNum, address, phone, email, card);
                clientRepo.Update(new Client(isIndividual, name, idNum, address, phone, email, card));
                log.Info(string.Format("The user with IdNum = {0} was updated.", idNum));
            }
            catch (ArgumentOutOfRangeException)
            {
                throw;
            }
        }
    }
}

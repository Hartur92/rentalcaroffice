﻿using LiberoRentACarClasses.DBAccess;
using LiberoRentACarClasses.Interfaces;
using log4net;
using log4net.Config;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses.Controllers
{
    public class RentController : IRentController
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        
        IRentRepository rentRepo;
        IClientRepository clientRepo;

        public RentController()
        {
            this.rentRepo = new RentRepository();
            this.clientRepo = new ClientRepository();
        }

        public List<Car> SearchNotRentedCars(int rank, string licensePlate)
        {
            List<Car> c = new List<Car>();
            var rents = rentRepo.GetRentsInGoing();
            var cars = rentRepo.GetCars(rank, licensePlate);
            //Search for the cars that was not rent
            foreach (var car in cars)
            {
                //if (!(rentSys.Rents.Any(x => x.Car.LicensePlate == car.LicensePlate && x.ReturnDate > DateTime.Now)))
                if (!rents.Contains(car.LicensePlate))
                {
                    c.Add(car);
                }
            }
            
            return c;
        }

        public void SaveRent(int userID, int carID, int days)
        {
            rentRepo.Add(new Rent(carID, userID, DateTime.Now, DateTime.Now.AddDays(days)));
            XmlConfigurator.Configure();
            log.Info(string.Format("The car {0} was rented by the user {1} for {2} days.", carID, userID, days));
            
        }

        public List<Rent> SearchRents(string name, string idNum)
        {
            return this.rentRepo.SearchRent(name, idNum);
        }

        public void Return(Rent rent, string mileage)
        {
            XmlConfigurator.Configure();
            int mileageInt = int.Parse(mileage);
            if (rent.MileageInit <= mileageInt)
            {
                rentRepo.Return(rent, mileageInt);
                log.Info(string.Format("The rent with RentID = {0}, UserID = {1} and CarID = {2} has finished at {3}", rent.RentID, rent.UserID, rent.CarID, DateTime.Now.ToString()));
            }
            else
            {
                log.Error(string.Format("An exception was lauched because the user's final mileage input ({0}) was invalid.",mileageInt));
                throw new ArgumentOutOfRangeException("Invalid mileage.");
            }
        }
    }
}

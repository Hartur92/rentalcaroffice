﻿using LiberoRentACarClasses.DBAccess;
using LiberoRentACarClasses.Interfaces;
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LiberoRentACarClasses.Controllers
{
    public class ModelController : IModelController
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        IManufacturersRepository manRepo;
        IModelRepository modelRepo;

        public ModelController()
        {
            this.manRepo = new ManufacturersRepository();
            this.modelRepo = new ModelRepository();
        }

        public List<string> GetModels(string manufacturer)
        {
            return modelRepo.GetModelByManufacturer(manufacturer);
        }

        public List<string> GetManufacturers()
        {
            return this.manRepo.GetManufacturers();
        }

        private bool ValidateModel(int manufacturerID, string name, string engine)
        {
            foreach (char ch in engine)
            {
                if ((ch < '0' || ch > '9') && ch != ',' && ch != '.')
                {
                    throw new ArgumentOutOfRangeException("Invalid egine.");
                }
            }

            if (name == "")
            {
                throw new ArgumentOutOfRangeException("Fill the name field.");
            }

            return true;
        }

        public void Add(int manufacturerID, string name, string engine)
        {
            XmlConfigurator.Configure();
            if (this.ValidateModel(manufacturerID, name, engine))
            {
                modelRepo.Add(new Model(manufacturerID, name, double.Parse(engine)));
                log.Info(string.Format("A model named {0} with ManufacturerID {1} was added.",name, manufacturerID));
            }   
        }

        public Model GetModel(string name)
        {
            return modelRepo.GetModel(name);
        }

        public void Update(string oldName, string name, string engine, string manufacturer)
        {
            XmlConfigurator.Configure();
            if (this.ValidateModel(manRepo.GetManufacturerID(manufacturer), name, engine))
            {
                Model model = new Model(manRepo.GetManufacturerID(manufacturer), name, double.Parse(engine));
                modelRepo.Update(oldName, model);
                log.Info(string.Format("The model named {0} was renamed to {1}.", oldName, name));
            }
        }
    }
}

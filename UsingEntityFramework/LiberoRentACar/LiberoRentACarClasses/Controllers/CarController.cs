﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiberoRentACarClasses.Interfaces;
using LiberoRentACarClasses.DBAccess;
using log4net;
using System.Reflection;
using log4net.Config;

namespace LiberoRentACarClasses.Controllers
{
    public class CarController : ICarController
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        ICarRepository carRepo;
        IModelRepository modelRepo;
        IManufacturersRepository manRepo;

        public CarController()
        {
            this.carRepo = new CarRepository();
            this.modelRepo = new ModelRepository();
            this.manRepo = new ManufacturersRepository();
        }

        public Array GetColors()
        {
            return Enum.GetValues(typeof(LiberoRentACarClasses.CarCor));
        }

        public bool ValidateCar(string model, string year, string mileage, string color, string licensePlate, bool isAutomatic, int rank)
        {
            XmlConfigurator.Configure();
            if (this.ValidadeLicensePlate(licensePlate))
            {
                if (mileage != "")
                {
                    return true;
                }
                else
                {
                    log.Error(string.Format("An exception was launched because the user's mileage input ({0}) was invalid.", mileage));
                    throw new ArgumentOutOfRangeException("Invalid mileage.");
                }
            }
            else
            {
                log.Error(string.Format("An exception was launched because the user's license plate input ({0}) was invalid.", licensePlate));
                throw new ArgumentOutOfRangeException("Invalid license plate.");
            }
        }

        public bool SaveCar(string model, string year, string mileage, string color, string licensePlate, bool isAutomatic, int rank, string price)
        {
            licensePlate = this.OnlyCharAndNumber(licensePlate);
            XmlConfigurator.Configure();
            try
            {
                if (this.ValidateCar(model, year, mileage, color, licensePlate, isAutomatic, rank))
                {
                    if (!carRepo.Contains(licensePlate))
                    {
                        this.carRepo.Add(
                        new Car(modelRepo.GetModel(model).ModelID,
                            int.Parse(year),
                            int.Parse(mileage),
                            ((CarCor)int.Parse(color)),
                            licensePlate,
                            isAutomatic,
                            rank, 
                            decimal.Parse(price)));
                        log.Info(string.Format("A car with license plate = {0} and Model = {1} was added.", licensePlate, model));
                        return true;
                    }
                    else
                    {
                        log.Error(string.Format("An exception was launch by the 'CarController' because the license plate {0} already exists.",licensePlate));
                        throw new ArgumentOutOfRangeException("This car's license plate already exists.");
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                throw;
            }
        }

        private string OnlyCharAndNumber(string str)
        {
            StringBuilder newStr = new StringBuilder();
            str = str.ToLower();
            foreach (char ch in str)
            {
                if ((ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'z'))
                    newStr.Append(ch);

            }
            return newStr.ToString();
        }

        private bool ValidadeLicensePlate(string plate)
        {
            int charCont = 0, numCont = 0;

            foreach (char ch in plate)
            {
                if (charCont > 3 || numCont > 3)
                    return false;
                else if (ch >= '0' && ch <= '9')
                    numCont++;
                else if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'))
                    charCont++;
                else
                    return false;
            }
            return true;
        }

        public void UpdateCar(string model, string year, string mileage, string color, string licensePlate, bool isAutomatic, int rank, string price)
        {
            XmlConfigurator.Configure();
            try
            {
                if (this.ValidateCar(model, year, mileage, color, licensePlate, isAutomatic, rank))
                {
                    carRepo.Update(new Car(modelRepo.GetModel(model).ModelID,
                            int.Parse(year),
                            int.Parse(mileage),
                            ((CarCor)int.Parse(color)),
                            licensePlate,
                            isAutomatic,
                            rank,
                            decimal.Parse(price)));
                    log.Info(string.Format("The car with license plate = {0} was updated.", licensePlate));
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                throw;
            }
        }

        public Car GetCar(int id)
        {
            return carRepo.Get(id);
        }

        public List<Car> GetCars(int rank, string licensePlate)
        {
            return carRepo.Get(rank, licensePlate);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace LiberoRentACarClasses
{
    public static class UtilsValidation
    {
        public static bool IsANumber(string c)
        {
            if ((c.First() < '0' || c.First() > '9') && !c.Contains("NumPad"))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}

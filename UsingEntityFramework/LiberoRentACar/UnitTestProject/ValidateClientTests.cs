﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LiberoRentACarClasses.Controllers;

namespace UnitTestProject
{
    [TestClass]
    public class ValidateClientTests
    {
        //Valid Input - only raw information into the fields
        [TestMethod]
        public void Validate_ValidInput()
        {
            //Arrange
            ClientController clientController = new ClientController();
            bool expected = true;
            bool found;
            
            //Act
            found = clientController.ValidateClient(true, "Hartur", "66238368373", "address", "34553594", "hbb@ecomp.poli.br", "456456456");

            //Assert
            Assert.AreEqual(expected, found);
        }

        //Valid Input with special caracters
        [TestMethod]
        public void Validate_ValidInputSpecial()
        {
            //Arrange
            ClientController clientController = new ClientController();
            bool expected = true;
            bool found;

            //Act
            found = clientController.ValidateClient(true, "Hartur Barreto Brito", "662.383.683-73", "address", "+55 (81) 3455-3594", "h-b_b@ecomp.poli.br", "456.456.456-456");

            //Assert
            Assert.AreEqual(expected, found);
        }

        //Test name validation
        [ExpectedException(typeof(ArgumentOutOfRangeException))]//Assert
        [TestMethod]
        public void Validate_Name1()
        {
            //Arrange
            ClientController clientController = new ClientController();

            //Act
            clientController.ValidateClient(true, "1Hartur", "662.383.683-73", "address", "+55 (81) 3455-3594", "h-b_b@ecomp.poli.br", "456.456.456-456");

        }

        [ExpectedException(typeof(ArgumentOutOfRangeException))]//Assert
        [TestMethod]
        public void Validate_Name2()
        {
            //Arrange
            ClientController clientController = new ClientController();

            //Act
            clientController.ValidateClient(true, "Har1tur", "662.383.683-73", "address", "+55 (81) 3455-3594", "h-b_b@ecomp.poli.br", "456.456.456-456");

        }

        [ExpectedException(typeof(ArgumentOutOfRangeException))]//Assert
        [TestMethod]
        public void Validate_Name3()
        {
            //Arrange
            ClientController clientController = new ClientController();

            //Act
            clientController.ValidateClient(true, "Hartur1", "662.383.683-73", "address", "+55 (81) 3455-3594", "h-b_b@ecomp.poli.br", "456.456.456-456");

        }

        //test cpf validation
        [ExpectedException(typeof(ArgumentOutOfRangeException))]//Assert
        [TestMethod]
        public void Validate_Cpf1()
        {
            //Arrange
            ClientController clientController = new ClientController();

            //Act
            clientController.ValidateClient(true, "Hartur", "a662.383.683-73", "address", "+55 (81) 3455-3594", "h-b_b@ecomp.poli.br", "456.456.456-456");

        }
        //test cpf validation - invalid cpf number
        [ExpectedException(typeof(ArgumentOutOfRangeException))]//Assert
        [TestMethod]
        public void Validate_Cpf2()
        {
            //Arrange
            ClientController clientController = new ClientController();

            //Act
            clientController.ValidateClient(true, "Hartur", "663.383.683-73", "address", "+55 (81) 3455-3594", "h-b_b@ecomp.poli.br", "456.456.456-456");

        }

        [ExpectedException(typeof(ArgumentOutOfRangeException))]//Assert
        [TestMethod]
        public void Validate_Cpf3()
        {
            //Arrange
            ClientController clientController = new ClientController();

            //Act
            clientController.ValidateClient(true, "Hartur", "663 383.683-73", "address", "+55 (81) 3455-3594", "h-b_b@ecomp.poli.br", "456.456.456-456");

        }

        [ExpectedException(typeof(ArgumentOutOfRangeException))]//Assert
        [TestMethod]
        public void Validate_Cpf4()
        {
            //Arrange
            ClientController clientController = new ClientController();

            //Act
            clientController.ValidateClient(true, "Hartur1", "662.383.683-7a", "address", "+55 (81) 3455-3594", "h-b_b@ecomp.poli.br", "456.456.456-456");

        }

        //Test address validation
        [ExpectedException(typeof(ArgumentOutOfRangeException))]//Assert
        [TestMethod]
        public void Validate_Address()
        {
            //Arrange
            ClientController clientController = new ClientController();

            //Act
            clientController.ValidateClient(true, "Hartur", "662.383.683-73", "", "+55 (81) 3455-3594", "h-b_b@ecomp.poli.br", "456.456.456-456");

        }

        //Test phone validation - right possibilities
        [TestMethod]
        public void Validate_Phone1()
        {
            //Arrange
            ClientController clientController = new ClientController();
            bool expected = true;
            bool found;

            //Act
            found = clientController.ValidateClient(true, "Hartur", "662.383.683-73", "address", "(81) 3455-3594", "h-b_b@ecomp.poli.br", "456.456.456-456");

            //Assert
            Assert.AreEqual(expected, found);
        }

        [TestMethod]
        public void Validate_Phone2()
        {
            //Arrange
            ClientController clientController = new ClientController();
            bool expected = true;
            bool found;

            //Act
            found = clientController.ValidateClient(true, "Hartur", "662.383.683-73", "address", "3455-3594", "h-b_b@ecomp.poli.br", "456.456.456-456");

            //Assert
            Assert.AreEqual(expected, found);
        }

        [TestMethod]
        public void Validate_Phone3()
        {
            //Arrange
            ClientController clientController = new ClientController();
            bool expected = true;
            bool found;

            //Act
            found = clientController.ValidateClient(true, "Hartur", "662.383.683-73", "address", "34553594", "h-b_b@ecomp.poli.br", "456.456.456-456");

            //Assert
            Assert.AreEqual(expected, found);
        }

        //Test phone validation - wrong possibilities
        [ExpectedException(typeof(ArgumentOutOfRangeException))]//Assert
        [TestMethod]
        public void Validate_Phone4()
        {
            //Arrange
            ClientController clientController = new ClientController();

            //Act
            clientController.ValidateClient(true, "Hartur", "662.383.683-73", "address", "a55 (81) 3455-3594", "h-b_b@ecomp.poli.br", "456.456.456-456");

        }

        [ExpectedException(typeof(ArgumentOutOfRangeException))]//Assert
        [TestMethod]
        public void Validate_Phone5()
        {
            //Arrange
            ClientController clientController = new ClientController();

            //Act
            clientController.ValidateClient(true, "Hartur1", "662.383.683-73", "address", "55 (81) 3455-3594a", "h-b_b@ecomp.poli.br", "456.456.456-456");
        }

        //Test email validation - Right possibilities
        [TestMethod]
        public void Validate_Email1()
        {
            //Arrange
            ClientController clientController = new ClientController();
            bool expected = true;
            bool found;

            //Act
            found = clientController.ValidateClient(true, "Hartur", "662.383.683-73", "address", "(81) 3455-3594", "hbb2@ecomp.poli.br", "456.456.456-456");

            //Assert
            Assert.AreEqual(expected, found);
        }

        [TestMethod]
        public void Validate_Email2()
        {
            //Arrange
            ClientController clientController = new ClientController();
            bool expected = true;
            bool found;

            //Act
            found = clientController.ValidateClient(true, "Hartur", "662.383.683-73", "address", "(81) 3455-3594", "h_b-b2.c@ecomp.poli.br", "456.456.456-456");

            //Assert
            Assert.AreEqual(expected, found);
        }

        //Test email validation - Wrong possibilities
        [ExpectedException(typeof(ArgumentOutOfRangeException))]//Assert
        [TestMethod]
        public void Validate_Email3()
        {
            //Arrange
            ClientController clientController = new ClientController();

            //Act
            clientController.ValidateClient(true, "Hartur", "662.383.683-73", "address", "(81) 3455-3594", "2hbb@ecomp.poli.br", "456.456.456-456");
        }

        //Test card validation
        [ExpectedException(typeof(ArgumentOutOfRangeException))]//Assert
        [TestMethod]
        public void Validate_Card1()
        {
            //Arrange
            ClientController clientController = new ClientController();

            //Act
            clientController.ValidateClient(true, "Hartur", "662.383.683-73", "address", "(81) 3455-3594", "h_b-b2.c@ecomp.poli.br", "a456.456.456-456");
        }

        [ExpectedException(typeof(ArgumentOutOfRangeException))]//Assert
        [TestMethod]
        public void Validate_Card2()
        {
            //Arrange
            ClientController clientController = new ClientController();

            //Act
            clientController.ValidateClient(true, "Hartur", "662.383.683-73", "address", "(81) 3455-3594", "h_b-b2.c@ecomp.poli.br", "456.4a56.456-456");
        }

        [ExpectedException(typeof(ArgumentOutOfRangeException))]//Assert
        [TestMethod]
        public void Validate_Card3()
        {
            //Arrange
            ClientController clientController = new ClientController();

            //Act
            clientController.ValidateClient(true, "Hartur", "662.383.683-73", "address", "(81) 3455-3594", "h_b-b2.c@ecomp.poli.br", "456.456.456-456a");
        }
        
    }
}

﻿using LiberoRentACarClasses;
using LiberoRentACarClasses.Controllers;
using LiberoRentACarClasses.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LiberoRentACarHome.CarWindows
{
    /// <summary>
    /// Interaction logic for UpdateCarSetStepWindow.xaml
    /// </summary>
    public partial class UpdateCarSetStepWindow : Window
    {
        ICarController carController;
        IManufacturerController manController;
        IModelController modelController;
        Car car;
        public UpdateCarSetStepWindow(int carId)
        {
            this.carController = new CarController();
            this.manController = new ManufacturerController();
            this.modelController = new ModelController();
            car = carController.GetCar(carId);
            InitializeComponent();


            Label_licensePlate.Content = car.LicensePlate;

            ComboBox_Color.ItemsSource = this.carController.GetColors();
            ComboBox_Color.SelectedIndex = (int)car.CarCor;

            ComboBox_Manufacturer.ItemsSource = this.manController.GetManufacturers();
            ComboBox_Manufacturer.SelectedValue = car.Model.Manufacturer.Name;

            ComboBox_Model.ItemsSource = this.modelController.GetModels(ComboBox_Manufacturer.SelectedItem.ToString());
            ComboBox_Model.SelectedValue = car.Model.Name;

            TxtBox_Mileage.Text = car.Mileage.ToString();
            TxtBox_Price.Text = car.Price.ToString();

            for (int i = -10; i <= 0; i++)
            {
                ComboBox_Year.Items.Add(DateTime.Now.Year + i);
            }
            ComboBox_Year.SelectedValue = car.Year;

            if ((car.Rank & 2) != 0)
                Checkbox_Economic.IsChecked = true;
            if ((car.Rank & 4) != 0)
                Checkbox_Intermidiate.IsChecked = true;
            if ((car.Rank & 8) != 0)
                Checkbox_Minivan.IsChecked = true;
            if ((car.Rank & 16) != 0)
                Checkbox_OffRoad.IsChecked = true;
            if ((car.Rank & 32) != 0)
                Checkbox_Premium.IsChecked = true;
            if ((car.Rank & 64) != 0)
                Checkbox_SUV.IsChecked = true;
            
        }

        private void Button_Click_MainMenu(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void Button_Click_Save(object sender, RoutedEventArgs e)
        {
            int rank = 0;
            //Sum of the rank dependin on the checkbox selected (id used and bitwise to compare the rak after)
            if ((bool)Checkbox_Economic.IsChecked)
                rank += 2;
            if ((bool)Checkbox_Intermidiate.IsChecked)
                rank += 4;
            if ((bool)Checkbox_Minivan.IsChecked)
                rank += 8;
            if ((bool)Checkbox_OffRoad.IsChecked)
                rank += 16;
            if ((bool)Checkbox_Premium.IsChecked)
                rank += 32;
            if ((bool)Checkbox_SUV.IsChecked)
                rank += 64;

            try
            {
                carController.UpdateCar(
                        ComboBox_Model.SelectedItem.ToString(),
                        ComboBox_Year.SelectedItem.ToString(),
                        TxtBox_Mileage.Text,
                        ComboBox_Color.SelectedIndex.ToString(),
                        Label_licensePlate.Content.ToString(),
                        Checkbox_Automatic.IsChecked.Value,
                        rank,
                        TxtBox_Price.Text
                        );
                MessageBox.Show("Successfully changed the car information.");
                MainWindow mainWindow = new MainWindow();
                mainWindow.Show();
                this.Close();
            }
            catch (ArgumentOutOfRangeException aore)
            {

                MessageBox.Show(aore.ParamName.ToString());
            }
        }

        private void ComboBox_Manufacturer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            using (RentSysDbContext rentSys = new RentSysDbContext())
            {
                ComboBox_Model.ItemsSource = this.modelController.GetModels(ComboBox_Manufacturer.SelectedItem.ToString());
                ComboBox_Model.SelectedIndex = 0;
            }
        }

        private void TxtBox_Mileage_KeyDown(object sender, KeyEventArgs e)
        {
            KeyConverter kc = new KeyConverter();

            if (UtilsValidation.IsANumber(kc.ConvertToString(e.Key)))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}

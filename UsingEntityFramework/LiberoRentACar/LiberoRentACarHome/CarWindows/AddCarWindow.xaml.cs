﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiberoRentACarClasses;
using LiberoRentACarClasses.Controllers;
using LiberoRentACarClasses.Interfaces;
using System.IO;

namespace LiberoRentACarHome
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class AddCarWindow : Window
    {
        //private RentSysDbContext rentSys;
        ICarController carController;
        IManufacturerController manController;
        IModelController modelController;

        public AddCarWindow()
        {
            //this.rentSys = new RentSysDbContext();
            InitializeComponent();
            this.carController = new CarController();
            this.manController = new ManufacturerController();
            this.modelController = new ModelController();
            //Get the possible values of colors to put into the combobox
            ComboBox_Color.ItemsSource = this.carController.GetColors();
            ComboBox_Color.SelectedIndex = 0;
            //Get the list of manufacturers and put into the combobox
            ComboBox_Manufacturer.ItemsSource = this.manController.GetManufacturers();
            ComboBox_Manufacturer.SelectedIndex = 0;
            //Get the list of models of a manufacturer and put into the combobox
            ComboBox_Model.ItemsSource = this.modelController.GetModels(ComboBox_Manufacturer.SelectedItem.ToString());
            ComboBox_Model.SelectedIndex = 0;
            
            //allow only add cars with 10 years or less
            for (int i = -10; i <= 0; i++)
            {
                ComboBox_Year.Items.Add(DateTime.Now.Year + i);
            }
            ComboBox_Year.SelectedIndex = 0;
        }
        //Button to return to the main menu
        private void Button_Click_MainMenu(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }
        //Button to save the information
        private void Button_Click_Save(object sender, RoutedEventArgs e)
        {
            int rank = 0;
            //Sum of the rank dependin on the checkbox selected (id used and bitwise to compare the rak after)
            if ((bool)Checkbox_Economic.IsChecked)
                rank += 2;
            if ((bool)Checkbox_Intermidiate.IsChecked)
                rank += 4;
            if ((bool)Checkbox_Minivan.IsChecked)
                rank += 8;
            if ((bool)Checkbox_OffRoad.IsChecked)
                rank += 16;
            if ((bool)Checkbox_Premium.IsChecked)
                rank += 32;
            if ((bool)Checkbox_SUV.IsChecked)
                rank += 64;
            //try catch block used to avoid pass the exception to the user
            try
            {
                if (this.carController.SaveCar(
                        ComboBox_Model.SelectedItem.ToString(),
                        ComboBox_Year.SelectedItem.ToString(), 
                        TxtBox_Mileage.Text, 
                        ComboBox_Color.SelectedIndex.ToString(), 
                        TxtBox_LicensePlate.Text.ToString(), 
                        Checkbox_Automatic.IsChecked.Value, 
                        rank,
                        TxtBox_Price.Text
                        ))
                    {        
                        //After save the car, is showed a message box and goes to the main menu window
                        MessageBox.Show("Success saved.");
                        MainWindow mainWindow = new MainWindow();
                        mainWindow.Show();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("There's already a car with this license plate registered.");
                    }
            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show(ex.ToString());
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show(ex.ParamName.ToString());
            }
        }

        private void ComboBox_Manufacturer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox_Model.ItemsSource = this.modelController.GetModels(ComboBox_Manufacturer.SelectedItem.ToString());
            ComboBox_Model.SelectedIndex = 0;
        }

        private void TxtBox_Mileage_KeyDown(object sender, KeyEventArgs e)
        {
            KeyConverter kc = new KeyConverter();

            if (UtilsValidation.IsANumber(kc.ConvertToString(e.Key)))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LiberoRentACarClasses;
using LiberoRentACarClasses.Controllers;
using LiberoRentACarClasses.Interfaces;

namespace LiberoRentACarHome
{
    /// <summary>
    /// Interaction logic for SelectClientWindow.xaml
    /// </summary>
    public partial class RentClientStepWindow : Window
    {
        private int carID;
        IRentController rentController;
        IClientController clientController;

        public RentClientStepWindow(int carID)
        {
            this.carID = carID;
            this.rentController = new RentController();
            this.clientController = new ClientController();
            
            InitializeComponent();

            for (int i = 1; i < 31; i++)
            {
                ComboBox_Time.Items.Add(i);
            }

            ComboBox_Time.SelectedIndex = 0;
        }

        private void Button_Click_MainMenu(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void Button_Click_Search(object sender, RoutedEventArgs e)
        {
            ListBox_Clients.ItemsSource = null;

            ListBox_Clients.ItemsSource = clientController.SearchClient(TxtBox_Name.Text, TxtBox_CPF.Text);
        }

        private void Button_Click_Rent(object sender, RoutedEventArgs e)
        {
            if (ListBox_Clients.SelectedItem == null){
                MessageBox.Show("Select some client.");
            }
            else
            {
                rentController.SaveRent(((Client)ListBox_Clients.SelectedItem).UserID, carID, int.Parse(ComboBox_Time.SelectedItem.ToString()));
                MessageBox.Show("The car was successfully rent.");
                MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
            }
        }

        private void ComboBox_Time_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ICarController carController = new CarController();
            Label_Price.Content = "R$ " + carController.GetCar(this.carID).Price * int.Parse(ComboBox_Time.SelectedItem.ToString());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiberoRentACarClasses;
using LiberoRentACarClasses.Controllers;
using LiberoRentACarClasses.Interfaces;

namespace LiberoRentACarHome
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ReturnWindow : Window
    {
        IRentController rentController;

        public ReturnWindow()
        {
            InitializeComponent();
            this.rentController = new RentController();
        }

        private void Button_Click_Search(object sender, RoutedEventArgs e)
        {
            ListBox_Rents.ItemsSource = null;

            ListBox_Rents.ItemsSource = rentController.SearchRents(TxtBox_Name.Text, TxtBox_CPF.Text);
        }

        private void Button_Click_Return(object sender, RoutedEventArgs e)
        {
            if (TxtBox_Mileage.Text == "" || ListBox_Rents.SelectedItem == null)
                MessageBox.Show("Fill the fields.");
            else
            {
                try
                {
                    rentController.Return((Rent)ListBox_Rents.SelectedItem, TxtBox_Mileage.Text);
                    MessageBox.Show("Successfully returned.");
                    MainWindow mainWindow = new MainWindow();
                    mainWindow.Show();
                    this.Close();
                }
                catch (ArgumentOutOfRangeException ex) 
                {
                    MessageBox.Show(ex.ParamName.ToString());
                }
            }

        }

        private void Button_Click_MainMenu(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void TxtBox_Mileage_KeyDown(object sender, KeyEventArgs e)
        {
            KeyConverter kc = new KeyConverter();

            if (UtilsValidation.IsANumber(kc.ConvertToString(e.Key)))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void ListBox_Rents_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int a = ((DateTime.Now - ((Rent)ListBox_Rents.SelectedItem).PickupDate).Days + 1);
            decimal b = ((Rent)ListBox_Rents.SelectedItem).Car.Price;
            Label_Price.Content = "R$ " + (((DateTime.Now - ((Rent)ListBox_Rents.SelectedItem).PickupDate).Days + 1) * ((Rent)ListBox_Rents.SelectedItem).Car.Price);
        }
    }
}

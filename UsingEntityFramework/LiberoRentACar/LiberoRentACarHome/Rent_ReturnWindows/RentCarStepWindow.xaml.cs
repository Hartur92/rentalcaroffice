﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiberoRentACarClasses.Controllers;
using LiberoRentACarClasses;
using LiberoRentACarClasses.Interfaces;

namespace LiberoRentACarHome
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class RentCarStepWindow : Window
    {
        IRentController rentController;
        public RentCarStepWindow()
        {
            this.rentController = new RentController();
            InitializeComponent();
        }

        private void Button_Click_Search(object sender, RoutedEventArgs e)
        {
            //Everytime when the search button is clicked, the list box is cleared
            ListBox_Cars.ItemsSource = null;

            //Sum the rank to search for the right car
            int rank = 0;
            if ((bool)Checkbox_Economic.IsChecked)
                rank += 2;
            if ((bool)Checkbox_Intermediate.IsChecked)
                rank += 4;
            if ((bool)Checkbox_Minivan.IsChecked)
                rank += 8;
            if ((bool)Checkbox_OffRoad.IsChecked)
                rank += 16;
            if ((bool)Checkbox_Premium.IsChecked)
                rank += 32;
            if ((bool)Checkbox_SUV.IsChecked)
                rank += 64;

            //Select the cars with the same Rank or license plate that is not rented
            var cars = rentController.SearchNotRentedCars(rank, TxtBox_Plate.Text);

            ListBox_Cars.ItemsSource = cars;
        }

        private void RadioButton_Cat_Checked(object sender, RoutedEventArgs e)
        {
            TxtBox_Plate.Visibility = System.Windows.Visibility.Hidden;
            Checkbox_Economic.Visibility = System.Windows.Visibility.Visible;
            Checkbox_Intermediate.Visibility = System.Windows.Visibility.Visible;
            Checkbox_Minivan.Visibility = System.Windows.Visibility.Visible;
            Checkbox_SUV.Visibility = System.Windows.Visibility.Visible;
            Checkbox_OffRoad.Visibility = System.Windows.Visibility.Visible;
            Checkbox_Premium.Visibility = System.Windows.Visibility.Visible;
            TxtBox_Plate.Text = "";
            Label_Search.Content = "Categories:";
        }

        private void RadioButton_Plate_Checked(object sender, RoutedEventArgs e)
        {
            Label_Search.Content = "License Plate:";
            TxtBox_Plate.Visibility = System.Windows.Visibility.Visible;
            Checkbox_Economic.Visibility = System.Windows.Visibility.Hidden;
            Checkbox_Intermediate.Visibility = System.Windows.Visibility.Hidden;
            Checkbox_Minivan.Visibility = System.Windows.Visibility.Hidden;
            Checkbox_SUV.Visibility = System.Windows.Visibility.Hidden;
            Checkbox_OffRoad.Visibility = System.Windows.Visibility.Hidden;
            Checkbox_Premium.Visibility = System.Windows.Visibility.Hidden;

            Checkbox_Economic.IsChecked = false;
            Checkbox_Intermediate.IsChecked = false;
            Checkbox_Minivan.IsChecked = false;
            Checkbox_SUV.IsChecked = false;
            Checkbox_OffRoad.IsChecked = false;
            Checkbox_Premium.IsChecked = false;
        }

        private void Button_Click_MainMenu(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void Button_Click_SelectClient(object sender, RoutedEventArgs e)
        {
            if (ListBox_Cars.SelectedItem == null){
                MessageBox.Show("Select some car.");
            }
            else
            {
                RentClientStepWindow selectClientWindow = new RentClientStepWindow(((Car)ListBox_Cars.SelectedItem).CarID);
                selectClientWindow.Show();
                this.Close();
            }
        }
    }
}

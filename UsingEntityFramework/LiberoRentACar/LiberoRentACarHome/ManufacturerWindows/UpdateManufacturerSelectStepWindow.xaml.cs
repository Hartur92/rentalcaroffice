﻿using LiberoRentACarClasses.Controllers;
using LiberoRentACarClasses.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LiberoRentACarHome.ManufacturerWindows
{
    /// <summary>
    /// Interaction logic for UpdateManufacturerSelectStepWindow.xaml
    /// </summary>
    public partial class UpdateManufacturerSelectStepWindow : Window
    {
        IManufacturerController manController;
        public UpdateManufacturerSelectStepWindow()
        {
            this.manController = new ManufacturerController();

            InitializeComponent();

            ComboBox_Manufacturer.ItemsSource = this.manController.GetManufacturers();
            ComboBox_Manufacturer.SelectedIndex = 0;
        }

        private void Button_Click_Change(object sender, RoutedEventArgs e)
        {
            UpdateManufacturerSetStepWindow update = new UpdateManufacturerSetStepWindow(ComboBox_Manufacturer.SelectedItem.ToString());
            update.Show();
            this.Close();
        }

        private void Button_Click_MainMenu(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }
    }
}

﻿using LiberoRentACarClasses.Controllers;
using LiberoRentACarClasses.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LiberoRentACarHome.ManufacturerWindows
{
    /// <summary>
    /// Interaction logic for UpdateManufacturerSetStepWindow.xaml
    /// </summary>
    public partial class UpdateManufacturerSetStepWindow : Window
    {
        IManufacturerController manController;
        string oldName;
        public UpdateManufacturerSetStepWindow(string name)
        {
            this.oldName = name;
            manController = new ManufacturerController();
            InitializeComponent();
            Txt_ManName.Text = name;
        }

        private void Button_Click_Save(object sender, RoutedEventArgs e)
        {
            try
            {
                manController.Update(oldName, Txt_ManName.Text);
                MessageBox.Show("The manufacturer's name was successfully updated.");
                MainWindow mainWindow = new MainWindow();
                mainWindow.Show();
                this.Close();
            }
            catch (ArgumentOutOfRangeException aore)
            {

                MessageBox.Show(aore.ParamName);
            }
            
        }

        private void Button_Click_MainMenu(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }
    }
}

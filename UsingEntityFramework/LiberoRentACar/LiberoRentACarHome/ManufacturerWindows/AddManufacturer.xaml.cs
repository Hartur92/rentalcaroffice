﻿using LiberoRentACarClasses.Controllers;
using LiberoRentACarClasses.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LiberoRentACarHome.ManufacturerWindows
{
    /// <summary>
    /// Interaction logic for AddManufacturer.xaml
    /// </summary>
    public partial class AddManufacturer : Window
    {
        IManufacturerController manCont;
        public AddManufacturer()
        {
            InitializeComponent();
            manCont = new ManufacturerController();
        }

        private void Button_Click_Save(object sender, RoutedEventArgs e)
        {
            try
            {
                manCont.Add(Txt_ManName.Text);
                MessageBox.Show("Manufacturer successfully added.");
                MainWindow main = new MainWindow();
                main.Show();
                this.Close();
            }
            catch (ArgumentOutOfRangeException aore)
            {
                MessageBox.Show(aore.ParamName);
            }
            
            
        }

        private void Button_Click_MainMenu(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }
    }
}

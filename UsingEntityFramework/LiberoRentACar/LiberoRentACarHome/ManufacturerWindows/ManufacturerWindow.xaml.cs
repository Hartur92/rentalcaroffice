﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LiberoRentACarHome.ManufacturerWindows
{
    /// <summary>
    /// Interaction logic for ManufacturerWindow.xaml
    /// </summary>
    public partial class ManufacturerWindow : Window
    {
        public ManufacturerWindow()
        {
            InitializeComponent();
        }

        private void Button_Click_AddMan(object sender, RoutedEventArgs e)
        {
            AddManufacturer addMan = new AddManufacturer();
            addMan.Show();
            this.Close();
        }

        private void Button_Click_UpdateMan(object sender, RoutedEventArgs e)
        {
            UpdateManufacturerSelectStepWindow manUpdate = new UpdateManufacturerSelectStepWindow();
            manUpdate.Show();
            this.Close();
        }

        private void Button_Click_MainMenu(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }
    }
}

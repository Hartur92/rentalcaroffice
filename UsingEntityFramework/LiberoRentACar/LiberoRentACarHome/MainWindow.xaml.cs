﻿using System.Windows;
using LiberoRentACarClasses;
using System.Linq;
using System.Data.Entity;
using LiberoRentACarHome.ManufacturerWindows;
using LiberoRentACarHome.ModelWindows;
using log4net;
using System.Reflection;
using log4net.Config;

namespace LiberoRentACarHome
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public MainWindow()
        {
            XmlConfigurator.Configure();
            Database.SetInitializer<RentSysDbContext>(new RentSysInitializer());
            
            InitializeComponent();
        }
        //Button to acces the clientID menu
        private void Button_Click_Client(object sender, RoutedEventArgs e)
        {
            //The object rentSys is passed to the other windows to help the data manipulation and avoid the use of the file
            //The object is sent as a reference to avoid memory consumption
            ClientWindow clientWindow = new ClientWindow();
            clientWindow.Show();
            this.Close();
        }
        //Button to acces the car menu
        private void Button_Click_Car(object sender, RoutedEventArgs e)
        {
            CarWindow carWindow = new CarWindow();
            carWindow.Show();
            this.Close();
        }
        //Button to start a rent process
        private void Button_Click_Rent(object sender, RoutedEventArgs e)
        {
            RentCarStepWindow rentWindow = new RentCarStepWindow();
            rentWindow.Show();
            this.Close();
        }
        //Button to start a return process
        private void Button_Click_Return(object sender, RoutedEventArgs e)
        {
            ReturnWindow returnWindow = new ReturnWindow();
            returnWindow.Show();
            this.Close(); ;
        }

        private void Button_Click_Man(object sender, RoutedEventArgs e)
        {
            ManufacturerWindow manWindow = new ManufacturerWindow();
            manWindow.Show();
            this.Close();
        }

        private void Button_Click_Model(object sender, RoutedEventArgs e)
        {
            ModelWindow model = new ModelWindow();
            model.Show();
            this.Close();
        }
    }
}

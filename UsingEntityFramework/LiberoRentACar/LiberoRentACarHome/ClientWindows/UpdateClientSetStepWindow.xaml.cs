﻿using LiberoRentACarClasses;
using LiberoRentACarClasses.Controllers;
using LiberoRentACarClasses.DBAccess;
using LiberoRentACarClasses.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LiberoRentACarHome
{
    /// <summary>
    /// Interaction logic for UpdateClientSetStepWindow.xaml
    /// </summary>
    public partial class UpdateClientSetStepWindow : Window
    {
        int clientID;
        IClientController clientControl;
        Client client;

        public UpdateClientSetStepWindow(int clientID)
        {
            this.clientControl = new ClientController();
            this.clientID = clientID;
            this.client = clientControl.GetClient(clientID);

            InitializeComponent();

            Txt_Address.Text = client.Address;
            Txt_Card.Text = client.Card;
            Txt_Email.Text = client.Email;
            Label_IdNumInfo.Content = client.IdNum;
            Txt_Name.Text = client.Name;
            Txt_Phone.Text = client.Phone;
        }

        private void Button_Click_MainMenu(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void OnlyNumbersOnKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            KeyConverter kc = new KeyConverter();

            if (UtilsValidation.IsANumber(kc.ConvertToString(e.Key)))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void Button_Click_Save(object sender, RoutedEventArgs e)
        {
            clientControl.UpdateClient(this.client.IsIndividual, Txt_Name.Text, Label_IdNumInfo.Content.ToString(), Txt_Address.Text, Txt_Phone.Text, Txt_Email.Text, Txt_Card.Text);
            MessageBox.Show("Successfully changed the client information.");
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }
    }
}

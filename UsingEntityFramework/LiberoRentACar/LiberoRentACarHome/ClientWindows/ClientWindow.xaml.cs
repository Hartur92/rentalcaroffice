﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiberoRentACarClasses;

namespace LiberoRentACarHome
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ClientWindow : Window
    {

        public ClientWindow()
        {
            InitializeComponent();
        }
        //Button to return to the main menu
        private void Button_Click_MainMenu(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }
        //Button to add a new clientID
        private void Button_Click_AddClient(object sender, RoutedEventArgs e)
        {
            AddClientWindow addWindow = new AddClientWindow();
            addWindow.Show();
            this.Close();
        }
        //Button to update clientID informations
        private void Button_Click_UpdateClient(object sender, RoutedEventArgs e)
        {
            UpdateClientSelectStepWindow updateClient = new UpdateClientSelectStepWindow();
            updateClient.Show();
            this.Close();
        }
    }
}

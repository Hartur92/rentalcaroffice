﻿using LiberoRentACarClasses;
using LiberoRentACarClasses.Controllers;
using LiberoRentACarClasses.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LiberoRentACarHome
{
    /// <summary>
    /// Interaction logic for UpdateClientSelectStepWindow.xaml
    /// </summary>
    public partial class UpdateClientSelectStepWindow : Window
    {
        IClientController clientController;

        public UpdateClientSelectStepWindow()
        {
            this.clientController = new ClientController();
            InitializeComponent();
        }

        private void Button_Click_MainMenu(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void Button_Click_Search(object sender, RoutedEventArgs e)
        {
            ListBox_Clients.ItemsSource = null;

            ListBox_Clients.ItemsSource = clientController.SearchClient(TxtBox_Name.Text, TxtBox_CPF.Text);
        }

        private void Button_Click_Change(object sender, RoutedEventArgs e)
        {
            UpdateClientSetStepWindow updateClientSetStep = new UpdateClientSetStepWindow(((Client)ListBox_Clients.SelectedItem).UserID);
            updateClientSetStep.Show();
            this.Close();
        }
    }
}

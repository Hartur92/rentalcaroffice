﻿using LiberoRentACarClasses;
using System;
using System.IO;
using System.Windows;
using System.Linq;
using LiberoRentACarClasses.Controllers;
using System.Windows.Input;
using LiberoRentACarClasses.Interfaces;

namespace LiberoRentACarHome
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class AddClientWindow : Window
    {
        IClientController addClientController;

        public AddClientWindow()
        {
            InitializeComponent();
            addClientController = new ClientController();
        }
        //When the radio button "individual" is selected, the labels are set to Name and CPF
        private void RadioButton_Checked_Individual(object sender, RoutedEventArgs e)
        {
            Label_Name.Content = "Name:";
            Label_IdNum.Content = "CPF:";
        }
        //When the radio button "legal" is selected, the labels are set to Enterprise and CNPJ
        private void RadioButton_Checked_Legal(object sender, RoutedEventArgs e)
        {
            Label_Name.Content = "Enterprise:";
            Label_IdNum.Content = "CNPJ:";
        }
        //Button used to get the informations and add to the persistence file
        private void Button_Click_Save(object sender, RoutedEventArgs e)
        {
            //try catch block to treat all the possible exceptions and try to not pass the error to the user
            try
            {
                this.addClientController.SaveClient(RadioButton_Individual.IsChecked.Value, Txt_Name.Text, Txt_IdNum.Text, Txt_Address.Text, Txt_Phone.Text, Txt_Email.Text, Txt_Card.Text);
                //Shows a successfully saved message
                MessageBox.Show("Success saved.");
                //After finish the save, the app return to the main menu
                MainWindow mainWindow = new MainWindow();
                mainWindow.Show();
                this.Close();
            }
            //This exception is launched when there's some problem during the validation of the informations into the fields
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show(ex.ParamName.ToString());
            }
        }
        //Button to return to the main menu
        private void Button_Click_MainMenu(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void OnlyNumbersOnKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            KeyConverter kc = new KeyConverter();

            if (UtilsValidation.IsANumber(kc.ConvertToString(e.Key)))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}

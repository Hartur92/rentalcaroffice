﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LiberoRentACarHome.ModelWindows
{
    /// <summary>
    /// Interaction logic for ModelWindow.xaml
    /// </summary>
    public partial class ModelWindow : Window
    {
        public ModelWindow()
        {
            InitializeComponent();
        }

        private void Button_Click_AddModel(object sender, RoutedEventArgs e)
        {
            AddModelWindow add = new AddModelWindow();
            add.Show();
            this.Close();
        }

        private void Button_Click_UpdateModel(object sender, RoutedEventArgs e)
        {
            UpdateModelSelectStepWindow update = new UpdateModelSelectStepWindow();
            update.Show();
            this.Close();
        }

        private void Button_Click_MainMenu(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }
    }
}

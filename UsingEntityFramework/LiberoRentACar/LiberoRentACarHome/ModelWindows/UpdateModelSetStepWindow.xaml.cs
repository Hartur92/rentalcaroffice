﻿using LiberoRentACarClasses;
using LiberoRentACarClasses.Controllers;
using LiberoRentACarClasses.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LiberoRentACarHome.ModelWindows
{
    /// <summary>
    /// Interaction logic for UpdateModelSetStepWindow.xaml
    /// </summary>
    public partial class UpdateModelSetStepWindow : Window
    {
        private string oldName;
        IModelController modelController;
        IManufacturerController manController;

        public UpdateModelSetStepWindow(string oldName)
        {
            this.modelController = new ModelController();
            this.oldName = oldName;
            this.manController = new ManufacturerController();

            Model old = modelController.GetModel(oldName);

            InitializeComponent();

            Txt_Name.Text = old.Name;
            Txt_Engine.Text = old.Engine.ToString();

            ComboBox_Man.ItemsSource = manController.GetManufacturers();
            ComboBox_Man.SelectedValue = old.Manufacturer.Name;
        }

        private void Button_Click_Save(object sender, RoutedEventArgs e)
        {
            try
            {
                modelController.Update(oldName, Txt_Name.Text, Txt_Engine.Text, ComboBox_Man.SelectedValue.ToString());
                MessageBox.Show("Model successfully updated.");
                MainWindow mainWindow = new MainWindow();
                mainWindow.Show();
                this.Close();
            }
            catch (ArgumentOutOfRangeException aore)
            {
                MessageBox.Show(aore.ParamName);
            }
        }

        private void Button_Click_MainMenu(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }
    }
}

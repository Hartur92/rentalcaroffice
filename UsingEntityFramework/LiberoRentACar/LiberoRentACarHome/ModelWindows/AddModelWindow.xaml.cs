﻿using LiberoRentACarClasses;
using LiberoRentACarClasses.Controllers;
using LiberoRentACarClasses.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LiberoRentACarHome.ModelWindows
{
    /// <summary>
    /// Interaction logic for AddModel.xaml
    /// </summary>
    public partial class AddModelWindow : Window
    {
        IModelController modelController;
        IManufacturerController manController;

        public AddModelWindow()
        {
            this.modelController = new ModelController();
            this.manController = new ManufacturerController();
            InitializeComponent();
            ComboBox_Man.ItemsSource = this.manController.GetManufacturers();
            ComboBox_Man.SelectedIndex = 0;
        }

        private void Button_Click_Save(object sender, RoutedEventArgs e)
        {
            try
            {
                modelController.Add(ComboBox_Man.SelectedIndex, Txt_Name.Text, Txt_Engine.Text);
                MessageBox.Show("Model successfully saved.");
                MainWindow mainWindow = new MainWindow();
                mainWindow.Show();
                this.Close();
            }
            catch (ArgumentOutOfRangeException aore)
            {
                MessageBox.Show(aore.ParamName);
            }
        }

        private void Button_Click_MainMenu(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }
    }
}

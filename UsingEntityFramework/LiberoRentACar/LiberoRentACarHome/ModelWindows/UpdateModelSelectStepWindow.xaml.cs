﻿using LiberoRentACarClasses.Controllers;
using LiberoRentACarClasses.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LiberoRentACarHome.ModelWindows
{
    /// <summary>
    /// Interaction logic for UpdateModelSelectStepWindow.xaml
    /// </summary>
    public partial class UpdateModelSelectStepWindow : Window
    {
        IManufacturerController manController;
        IModelController modelController;

        public UpdateModelSelectStepWindow()
        {
            InitializeComponent();
            this.manController = new ManufacturerController();
            this.modelController = new ModelController();

            //Get the list of manufacturers and put into the combobox
            ComboBox_Manufacturer.ItemsSource = this.manController.GetManufacturers();
            ComboBox_Manufacturer.SelectedIndex = 0;
            //Get the list of models of a manufacturer and put into the combobox
            ComboBox_Model.ItemsSource = this.modelController.GetModels(ComboBox_Manufacturer.SelectedItem.ToString());
            ComboBox_Model.SelectedIndex = 0;
        }

        private void ComboBox_Manufacturer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox_Model.ItemsSource = this.modelController.GetModels(ComboBox_Manufacturer.SelectedItem.ToString());
            ComboBox_Model.SelectedIndex = 0;
        }

        private void Button_Click_Change(object sender, RoutedEventArgs e)
        {
            UpdateModelSetStepWindow update = new UpdateModelSetStepWindow(ComboBox_Model.Text);
            update.Show();
            this.Close();
        }

        private void Button_Click_MainMenu(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }
    }
}

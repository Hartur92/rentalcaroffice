﻿#pragma checksum "..\..\..\CarWindows\AddCarWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "81799EE9CA993F4DA4D1454082C99452"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace LiberoRentACarHome {
    
    
    /// <summary>
    /// AddCarWindow
    /// </summary>
    public partial class AddCarWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 15 "..\..\..\CarWindows\AddCarWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtBox_Mileage;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\..\CarWindows\AddCarWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtBox_LicensePlate;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\CarWindows\AddCarWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtBox_Price;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\CarWindows\AddCarWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ComboBox_Manufacturer;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\CarWindows\AddCarWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ComboBox_Model;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\CarWindows\AddCarWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ComboBox_Year;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\CarWindows\AddCarWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ComboBox_Color;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\CarWindows\AddCarWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox Checkbox_Automatic;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\CarWindows\AddCarWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox Checkbox_Economic;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\CarWindows\AddCarWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox Checkbox_Intermidiate;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\..\CarWindows\AddCarWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox Checkbox_Minivan;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\CarWindows\AddCarWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox Checkbox_OffRoad;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\CarWindows\AddCarWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox Checkbox_Premium;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\CarWindows\AddCarWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox Checkbox_SUV;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/LiberoRentACarHome;component/carwindows/addcarwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\CarWindows\AddCarWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.TxtBox_Mileage = ((System.Windows.Controls.TextBox)(target));
            
            #line 15 "..\..\..\CarWindows\AddCarWindow.xaml"
            this.TxtBox_Mileage.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxtBox_Mileage_KeyDown);
            
            #line default
            #line hidden
            return;
            case 2:
            this.TxtBox_LicensePlate = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.TxtBox_Price = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.ComboBox_Manufacturer = ((System.Windows.Controls.ComboBox)(target));
            
            #line 19 "..\..\..\CarWindows\AddCarWindow.xaml"
            this.ComboBox_Manufacturer.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.ComboBox_Manufacturer_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 5:
            this.ComboBox_Model = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 6:
            this.ComboBox_Year = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 7:
            this.ComboBox_Color = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 8:
            this.Checkbox_Automatic = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 9:
            this.Checkbox_Economic = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 10:
            this.Checkbox_Intermidiate = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 11:
            this.Checkbox_Minivan = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 12:
            this.Checkbox_OffRoad = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 13:
            this.Checkbox_Premium = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 14:
            this.Checkbox_SUV = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 15:
            
            #line 33 "..\..\..\CarWindows\AddCarWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click_Save);
            
            #line default
            #line hidden
            return;
            case 16:
            
            #line 34 "..\..\..\CarWindows\AddCarWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click_MainMenu);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

